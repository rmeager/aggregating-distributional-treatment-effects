# multivariate normal fit to consumerdurables 
# Rachael Meager
# April 2016 

### Notes

### Preliminaries 

# clear the workspace to avoid gremlins and past globals from past irresponsible scripts
# but we can't do this if the masterfile is being used to run the script, so we check that first:
if(exists("masterfile_run") == "FALSE"){
  rm(list = ls())
}

# install and load packages

installation_needed  <- FALSE
loading_needed <- TRUE
package_list <- c('ggplot2', 'rstan','reshape','reshape2','coda','xtable', 'dplyr', 'Runuran', 'testthat',
                  "MCMCpack", "geoR", "gtools", 'gPdtest', 'fBasics',"PtProcess", "VGAM", "MASS","quantreg",
                  "boot")
if(installation_needed){install.packages(package_list, repos='http://cran.us.r-project.org')}
if(loading_needed){lapply(package_list, require, character.only = TRUE)}

# load consumerdurables data
load("data/microcredit_project_data.RData")
if( exists("USD_convert_to_2009_dollars")!=TRUE){stop("OLD DATA FILE LOADED! RELOAD CORRECT FILE!")}


# This preps data so that the model can be written efficiently - it's not necessary but it's the best way I know to code it in STAN
site <- c(attanasio_indicator, augsberg_indicator,banerjee_indicator, crepon_indicator)-1
consumerdurables <- c(attanasio_consumerdurables, augsberg_consumerdurables,banerjee_consumerdurables, crepon_consumerdurables)
treatment <- c(attanasio_treatment, augsberg_treatment,banerjee_treatment, crepon_treatment)
priorbiz <- c(attanasio_existingbusiness, augsberg_existingbusiness, banerjee_existingbusiness, crepon_existingbusiness)

# Now we have to standardise any variables which are in local currency units to USD PPP per fortnight in 2009 dollars


expanded_standardiser_USD_PPP_per_fortnight <- c( rep(the_consumerdurables_standardiser_USD_PPP_per_fortnight[1],length(attanasio_indicator)),
                                                  rep(the_consumerdurables_standardiser_USD_PPP_per_fortnight[2],length(augsberg_indicator)),
                                                  rep(the_consumerdurables_standardiser_USD_PPP_per_fortnight[3],length(banerjee_indicator)),
                                                  rep(the_consumerdurables_standardiser_USD_PPP_per_fortnight[4],length(crepon_indicator)))

consumerdurables <- consumerdurables*expanded_standardiser_USD_PPP_per_fortnight

# bind everything into a data frame
data <- data.frame(site, consumerdurables, treatment, priorbiz)

# We gotta remove the NA values
data <- data[complete.cases(data),]

# dither
data$consumerdurables <- dither(data$consumerdurables, type = "right", value = 1)


# define the function that does the full quantile analysis

multinorm_quantile_aggregation_function <- function(quantiles_list, N, K, data){
  
  # check that the data is the right format
  if(!"site" %in% colnames(data) | !"treatment" %in% colnames(data) | !"consumerdurables" %in% colnames(data)  ){stop("data must be a dataframe with columns including site, treatment and consumerdurables")}

# now quantile reg!! 
base_quantile_picker <- function(quantile_reg){
  quantile_reg$coef[1,1]
}

base_quantile_se_picker <- function(quantile_reg){
  quantile_reg$coef[1,2]
}
te_picker <- function(quantile_reg){
  quantile_reg$coef[2,1]
}
se_picker <- function(quantile_reg){
  quantile_reg$coef[2,2]
}  

quantile_reg <- list()
quantile_summary <- list()
y_0 <- matrix(NA,K,N)
y_0_se <- matrix(NA,K,N)

for(k in 1:K){
  quantile_reg[[k]] <- rq(data$consumerdurables[(data$site==k)&(data$treatment==0)] ~ 1, tau = quantiles_list)
  
  quantile_summary[[k]] <- summary(quantile_reg[[k]],se = "iid")
  
  y_0[k,] <- unlist(lapply(quantile_summary[[k]],base_quantile_picker))
  y_0_se[k,] <- unlist(lapply(quantile_summary[[k]], base_quantile_se_picker))
  
} # closes forloop indexed by k

quantile_reg <- list()
quantile_summary <- list()
y_1 <- matrix(NA,K,N)
y_1_se <- matrix(NA,K,N)

for(k in 1:K){
  quantile_reg[[k]] <- rq(data$consumerdurables[(data$site==k)&(data$treatment==1)] ~ 1, tau = quantiles_list)
  
  quantile_summary[[k]] <- summary(quantile_reg[[k]],se = "iid")
  
  y_1[k,] <- unlist(lapply(quantile_summary[[k]],base_quantile_picker))
  y_1_se[k,] <- unlist(lapply(quantile_summary[[k]], base_quantile_se_picker))
  
} # closes forloop indexed by k

quantile_reg <- list()
quantile_summary <- list()
beta_1 <- matrix(NA,K,N)
beta_1_se <- matrix(NA,K,N)

for(k in 1:K){
  quantile_reg[[k]] <- rq(data$consumerdurables[(data$site==k)] ~ data$treatment[(data$site==k)], tau = quantiles_list)
  
  quantile_summary[[k]] <- summary(quantile_reg[[k]],se = "iid")
  
  beta_1[k,] <- unlist(lapply(quantile_summary[[k]],te_picker))
  beta_1_se[k,] <- unlist(lapply(quantile_summary[[k]], se_picker))
  
} # closes forloop indexed by k


# testing for my own edification
quantile_reg <- list()
quantile_summary <- list()


for(k in 1:K){
  quantile_reg[[k]] <- rq(data$consumerdurables[(data$site==k)] ~ data$treatment[(data$site==k)] , tau = quantiles_list)
  
  quantile_summary[[k]] <- summary(quantile_reg[[k]],se = "iid")

  
} # closes forloop indexed by k



print(y_0)
print(y_1)
print(y_0_se)
print(y_1_se)
print(y_1-y_0)

# now STAN MODEL! 
# fit the model 
fit_data <- list(N = N,
                 K = K,
                 theta = quantiles_list,
                 y_0 = y_0,
                 y_1 = y_1,
                 y_0_se = y_0_se,
                 y_1_se = y_1_se,
                 prior_dispersion_on_beta_0 = 1000*diag(N),
                 prior_dispersion_on_beta_1 = 1000*diag(N))

stan_fit <- stan("stan code/hierarchical-multivariate-normal-differences-in-quantiles.stan",
                 iter = 20000, cores = 4, data = fit_data, control = list(adapt_delta=0.9, max_treedepth = 12))

print(stan_fit)

stan_fit_summary <- summary(stan_fit)
stan_fit_table <- xtable(stan_fit_summary$summary) 
return(stan_fit)
} # closes function



# denote quantiles of interest
quantiles_list <- seq(0.05, 0.95,0.10)
N <- as.numeric(length(quantiles_list))
K <- as.numeric(length(unique(site)))


# run analysis split by pb

stan_fit_pb_0 <- multinorm_quantile_aggregation_function(quantiles_list, N, K, data[data$priorbiz==0,])
stan_fit_pb_1 <- multinorm_quantile_aggregation_function(quantiles_list, N, K, data[data$priorbiz==1,])


save.image("output/multivariate_normal_differences_quantile_model_hierarchical_data_consumerdurables_output_pb_split.RData")







