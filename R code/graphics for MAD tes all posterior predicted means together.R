# graphics for all parent mean observations
# Rachael Meager
# First Version: Sept 2015

### Notes ###

# Analysis needs to be done before you can run this, obviously

### Preliminaries and Data Intake ###
# clear the workspace to avoid gremlins and past globals from past irresponsible scripts
# but we can't do this if the masterfile is being used to run the script, so we check that first:
if(exists("masterfile_run") == "FALSE"){
  rm(list = ls())
}

installation_needed  <- FALSE
loading_needed <- TRUE
package_list <- c('ggplot2', 'rstan','reshape','reshape2','coda','xtable', 'dplyr', 'Runuran', 'testthat',
                  "MCMCpack", "geoR", "gtools", 'gPdtest', 'fBasics',"PtProcess", "VGAM", "MASS","quantreg",
                  "boot", "stargazer")
if(installation_needed){install.packages(package_list, repos='http://cran.us.r-project.org')}
if(loading_needed){lapply(package_list, require, character.only = TRUE)}


# some functions and variables
study_country <- c("Mexico", "Mongolia", "Bosnia", "India", "Morocco", "Philippines", "Ethiopia")
study_country_truncated <- c("Mexico", "Mongolia", "Bosnia", "India", "Morocco")


tau_name <- c("posterior_predicted_theta_k[4]")

country <- study_country

# define function
FE_estimator <- function(taus, ses){
  inverse_vars <- 1/ (ses^2)
  inverse_var_tot <- sum(inverse_vars)
  inverse_var_weight <- inverse_vars/inverse_var_tot
  FE_mean <- sum(taus* inverse_var_weight)
  FE_se <- sqrt(1/inverse_var_tot)
  FE_output <- c(FE_mean,FE_se)
  return(FE_output)
}


# Load data
load("output/microcredit_MAD_mean_effects_profit.RData")
textable_profit_variance_joint <- stan_fit_table
mad_mean_diff_est <- FE_estimator(mad_mean_diff_est, mad_mean_diff_se)[1]
mad_mean_diff_se <- FE_estimator(mad_mean_diff_est, mad_mean_diff_se)[2]
textable_profit_variance_independent_full_pooling <- data.frame(mad_mean_diff_est, 
                                                              mad_mean_diff_est - 1.96*mad_mean_diff_se,
                                                              mad_mean_diff_est - 0.67*mad_mean_diff_se,
                                                              mad_mean_diff_est + 0.67*mad_mean_diff_se,
                                                              mad_mean_diff_est + 1.96*mad_mean_diff_se)
colnames(textable_profit_variance_independent_full_pooling) <- c("mean", "X2.5.", "X25." ,"X75.","X97.5.") 
rownames(textable_profit_variance_independent_full_pooling) <- tau_name

load("output/microcredit_MAD_mean_effects_expenditures.RData")
textable_expenditures_variance_joint <- stan_fit_table
mad_mean_diff_est <- FE_estimator(mad_mean_diff_est, mad_mean_diff_se)[1]
mad_mean_diff_se <- FE_estimator(mad_mean_diff_est, mad_mean_diff_se)[2]
textable_expenditures_variance_independent_full_pooling <- data.frame(mad_mean_diff_est, 
                                                                    mad_mean_diff_est - 1.96*mad_mean_diff_se,
                                                                    mad_mean_diff_est - 0.67*mad_mean_diff_se,
                                                                    mad_mean_diff_est + 0.67*mad_mean_diff_se,
                                                                    mad_mean_diff_est + 1.96*mad_mean_diff_se)
colnames(textable_expenditures_variance_independent_full_pooling) <- c("mean", "X2.5.", "X25." ,"X75.","X97.5.")
rownames(textable_expenditures_variance_independent_full_pooling) <- tau_name

load("output/microcredit_MAD_mean_effects_revenues.RData")
textable_revenues_variance_joint <- stan_fit_table
mad_mean_diff_est <- FE_estimator(mad_mean_diff_est, mad_mean_diff_se)[1]
mad_mean_diff_se <- FE_estimator(mad_mean_diff_est, mad_mean_diff_se)[2]
textable_revenues_variance_independent_full_pooling <- data.frame(mad_mean_diff_est, 
                                                                mad_mean_diff_est - 1.96*mad_mean_diff_se,
                                                                mad_mean_diff_est - 0.67*mad_mean_diff_se,
                                                                mad_mean_diff_est + 0.67*mad_mean_diff_se,
                                                                mad_mean_diff_est + 1.96*mad_mean_diff_se)
colnames(textable_revenues_variance_independent_full_pooling) <- c("mean", "X2.5.", "X25." ,"X75.","X97.5.")
rownames(textable_revenues_variance_independent_full_pooling) <- tau_name

load("output/microcredit_MAD_mean_effects_consumption.RData")
textable_consumption_variance_joint <- stan_fit_table
mad_mean_diff_est <- FE_estimator(mad_mean_diff_est, mad_mean_diff_se)[1]
mad_mean_diff_se <- FE_estimator(mad_mean_diff_est, mad_mean_diff_se)[2]
textable_consumption_variance_independent_full_pooling <- data.frame(mad_mean_diff_est, 
                                                                   mad_mean_diff_est - 1.96*mad_mean_diff_se,
                                                                   mad_mean_diff_est - 0.67*mad_mean_diff_se,
                                                                   mad_mean_diff_est + 0.67*mad_mean_diff_se,
                                                                   mad_mean_diff_est + 1.96*mad_mean_diff_se)
colnames(textable_consumption_variance_independent_full_pooling) <- c("mean", "X2.5.", "X25." ,"X75.","X97.5.")
rownames(textable_consumption_variance_independent_full_pooling) <- tau_name

load("output/microcredit_MAD_mean_effects_consumerdurables.RData")
textable_consumerdurables_variance_joint <- stan_fit_table
mad_mean_diff_est <- FE_estimator(mad_mean_diff_est, mad_mean_diff_se)[1]
mad_mean_diff_se <- FE_estimator(mad_mean_diff_est, mad_mean_diff_se)[2]
textable_consumerdurables_variance_independent_full_pooling <- data.frame(mad_mean_diff_est, 
                                                                        mad_mean_diff_est - 1.96*mad_mean_diff_se,
                                                                        mad_mean_diff_est - 0.67*mad_mean_diff_se,
                                                                        mad_mean_diff_est + 0.67*mad_mean_diff_se,
                                                                        mad_mean_diff_est + 1.96*mad_mean_diff_se)
colnames(textable_consumerdurables_variance_independent_full_pooling) <- c("mean", "X2.5.", "X25." ,"X75.","X97.5.")
rownames(textable_consumerdurables_variance_independent_full_pooling) <- tau_name

load("output/microcredit_MAD_mean_effects_temptation.RData")
textable_temptation_variance_joint <- stan_fit_table
mad_mean_diff_est <- FE_estimator(mad_mean_diff_est, mad_mean_diff_se)[1]
mad_mean_diff_se <- FE_estimator(mad_mean_diff_est, mad_mean_diff_se)[2]
textable_temptation_variance_independent_full_pooling <- data.frame(mad_mean_diff_est, 
                                                                  mad_mean_diff_est - 1.96*mad_mean_diff_se,
                                                                  mad_mean_diff_est - 0.67*mad_mean_diff_se,
                                                                  mad_mean_diff_est + 0.67*mad_mean_diff_se,
                                                                  mad_mean_diff_est + 1.96*mad_mean_diff_se)
colnames(textable_temptation_variance_independent_full_pooling) <- c("mean", "X2.5.", "X25." ,"X75.","X97.5.")
rownames(textable_temptation_variance_independent_full_pooling) <- tau_name

## JOINT MODEL ###

outcome_variable <- c("Profit", "Expenditure", "Revenue", "Consumption", "Consumer Durables", "Temptation Goods")
textable_tau_set <- rbind(textable_profit_variance_joint[tau_name,],
                          textable_expenditures_variance_joint[tau_name,],
                          textable_revenues_variance_joint[tau_name,],
                          textable_consumption_variance_joint[tau_name,],
                          textable_consumerdurables_variance_joint[tau_name,],
                          textable_temptation_variance_joint[tau_name,])


tau_df  <- data.frame(outcome_variable, textable_tau_set)

tau_k_plot <- ggplot(data=tau_df, aes(x=outcome_variable, y=mean, colour=outcome_variable, fill = outcome_variable), alpha= 0.2, width=.2) + coord_cartesian(ylim = c(-30,30)) 
tau_k_plot_fancy <- tau_k_plot +  geom_boxplot(aes(ymax=X97.5., ymin=X2.5., lower =X25. , upper = X75., middle = mean,  alpha= 0.2, width=.2), stat=('identity'))


textable_taup_set <- rbind(textable_profit_variance_independent_full_pooling[tau_name,],
                           textable_expenditures_variance_independent_full_pooling[tau_name,],
                           textable_revenues_variance_independent_full_pooling[tau_name,],
                           textable_consumption_variance_independent_full_pooling[tau_name,],
                           textable_consumerdurables_variance_independent_full_pooling[tau_name,],
                           textable_temptation_variance_independent_full_pooling[tau_name,])



taup_df  <- data.frame(outcome_variable, textable_taup_set)

taup_k_plot <- ggplot(data=taup_df, aes(x=outcome_variable, y=mean, colour=outcome_variable, fill = outcome_variable), alpha= 0.2, width=.2) + coord_cartesian(ylim = c(-30,30)) 
taup_k_plot_fancy <- taup_k_plot +  geom_boxplot(aes(ymax=X97.5., ymin=X2.5., lower =X25. , upper = X75., middle = mean,  alpha= 0.2, width=.2), stat=('identity'))

estimate_type <- c(rep("BHM Posterior",length(outcome_variable)), rep("Full Pooling Model",length(outcome_variable)))
estimate_outcome_variable <- rep(outcome_variable,2)
estimate_means <- c(tau_df$mean, taup_df$mean)
estimate_ses <- c(tau_df$sd, taup_df$sd)
estimate_X2.5. <- c(tau_df$X2.5., taup_df$X2.5.)
estimate_X97.5. <- c(tau_df$X97.5.,taup_df$X97.5. )
estimate_X25. <- c(tau_df$X25.,taup_df$X25. )
estimate_X75. <- c(tau_df$X75.,taup_df$X75. )

estimates_df <- data.frame(estimate_type, estimate_outcome_variable, estimate_means, estimate_ses, estimate_X2.5., estimate_X25., estimate_X75., estimate_X97.5.)


# The palette with black:

pb_posterior_plot <- ggplot(data=estimates_df, aes(x=factor(estimate_outcome_variable), y=estimate_means, color = factor(estimate_type), fill  = factor(estimate_type), width=.4,  alpha= 0.25))  

pb_posterior_fancy_plot <- pb_posterior_plot +  geom_boxplot(aes(ymax=estimate_X97.5., ymin=estimate_X2.5., lower=estimate_X25. , upper = estimate_X75., middle = estimate_means), stat=('identity'))



pdf(file="output/MAD_te_posterior_predicted_mean_all_outcome_variables_joint.pdf",width = 10, height = 6)
pb_posterior_fancy_plot + coord_flip(ylim=c(-1,1)) +ggtitle("Posterior predicted treatment effect on the MAD \n Joint Model (exponentiated multiplicative specification)")  +theme(legend.position="bottom") +ylab("Posterior mean, 50% interval (box), and 95% interval (line) \n for each exponentiated multiplicative Treatment Effect") + xlab("") + theme(legend.title=element_blank()) + scale_alpha_identity() + theme(text = element_text(size=14,color="black"), axis.text.y = element_text(size=16,color="grey40"),axis.text.x = element_text(size=16,color="grey40"))
dev.off()

sink("output/MAD_TE_posterior_predicted_means_marginal_posteriors_joint.txt")
stargazer(estimates_df, summary=FALSE, title="Marginal Posterior Predicted Dists for MAD Treatment Effects: Joint Model")
sink()

