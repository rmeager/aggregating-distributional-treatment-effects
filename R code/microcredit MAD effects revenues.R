
# MAD model efficiently written
# Rachael Meager
# First Version: Sept 2015

### Notes ###

# All data was publicly available from the authors, without whom this project would not be possible. Hooray for open science!
# I use "i" as the general index for household-level variables and "k" as the index for site-level variables


# A note about NAs: I have tried to keep NAs as NAs, except in TWO CASES. 
# 1. revenues/Business expenses/Revenues. An NA in here can indicate not operating a business and we want to capture that in a zero.
# 2. Variables constructed by summing other variables should not be NA unless *ALL* of the constituent variables are NA.


### Preliminaries ###
# clear the workspace to avoid gremlins and past globals from past irresponsible scripts
# but we can't do this if the masterfile is being used to run the script, so we check that first:
if(exists("masterfile_run") == "FALSE"){
  rm(list = ls())
}

# install and load packages


installation_needed  <- FALSE
loading_needed <- TRUE
package_list <- c('ggplot2', 'rstan','reshape','reshape2','coda','xtable', 'dplyr', 'Runuran', 'testthat',
                  "MCMCpack", "geoR", "gtools", 'gPdtest', 'fBasics',"PtProcess", "VGAM", "MASS","quantreg",
                  "boot", "multicore", 'lsr', "boot")
if(installation_needed){install.packages(package_list, repos='http://cran.us.r-project.org')}
if(loading_needed){lapply(package_list, require, character.only = TRUE)}

## Load the data ##

load("data/microcredit_project_data.RData")
if( exists("USD_convert_to_2009_dollars")=="FALSE"){stop("OLD DATA FILE LOADED! RELOAD CORRECT FILE!")}

### VARIABLE STACKING PROCEDURE FOR COMPUATIONAL EASE ###

# This preps data so that the model can be written efficiently - it's not necessary but it's the best way I know to code it in STAN
site <- c(angelucci_indicator, attanasio_indicator, augsberg_indicator,banerjee_indicator, crepon_indicator, karlan_indicator, tarozzi_indicator)
revenues <- c(angelucci_revenues, attanasio_revenues, augsberg_revenues,banerjee_revenues, crepon_revenues, karlan_revenues, tarozzi_revenues)
treatment <- c(angelucci_treatment, attanasio_treatment, augsberg_treatment,banerjee_treatment, crepon_treatment, karlan_treatment, tarozzi_treatment)

# Now we have to standardise any variables which are in local currency units to USD PPP per fortnight in 2009 dollars


expanded_standardiser_USD_PPP_per_fortnight <- c( rep(the_revenues_standardiser_USD_PPP_per_fortnight[1],length(angelucci_indicator)),
                                                  rep(the_revenues_standardiser_USD_PPP_per_fortnight[2],length(attanasio_indicator)),
                                                  rep(the_revenues_standardiser_USD_PPP_per_fortnight[3],length(augsberg_indicator)),
                                                  rep(the_revenues_standardiser_USD_PPP_per_fortnight[4],length(banerjee_indicator)),
                                                  rep(the_revenues_standardiser_USD_PPP_per_fortnight[5],length(crepon_indicator)),
                                                  rep(the_revenues_standardiser_USD_PPP_per_fortnight[6],length(karlan_indicator)),
                                                  rep(the_revenues_standardiser_USD_PPP_per_fortnight[7],length(tarozzi_indicator)))

revenues <- revenues*expanded_standardiser_USD_PPP_per_fortnight
revenues <- dither(revenues, type = "symmetric", value = 10)
# bind everything into a data frame
data <- data.frame(site, revenues, treatment)

# We gotta remove the NA values
data <- data[complete.cases(data),]

# get K 
 K <- as.numeric(length(unique(data$site)))

### COMPUTING MEAN ABS DEVIATION FROM MEAN ("mad_mean") ###

mad_mean_control <- rep(NA, K)
mad_mean_treatment <- rep(NA, K)
mad_mean_TE <- rep(NA, K)
exp_mad_mean_control <- rep(NA, K)
exp_mad_mean_TE <- rep(NA, K)

for(i in 1:K){
  mad_mean_control[i] <- aad(data$revenues[data$site==i & data$treatment==0])
  mad_mean_treatment[i] <- aad(data$revenues[data$site==i & data$treatment==1]) 
  mad_mean_TE[i] <- mad_mean_treatment[i] - mad_mean_control[i] 
  exp_mad_mean_control[i] <- log(mad_mean_control[i])
  exp_mad_mean_TE[i] <- log(mad_mean_treatment[i]) - log(mad_mean_control[i])
}

mad_mean_TE

# to do the bootstap we need to define these as statistics 
exp_diff_mad_mean_stat <- function(data, indices){
  data <- data[indices,] # allows boot to select sample 
  mad_mean_control <- aad(data$revenues[data$treatment==0])
  mad_mean_treatment <- aad(data$revenues[data$treatment==1]) 
  mad_mean_TE <- mad_mean_treatment - mad_mean_control 
  exp_mad_mean_control <- log(mad_mean_control)
  exp_mad_mean_TE <- log(mad_mean_treatment) - log(mad_mean_control)
  return(exp_mad_mean_TE)
}

exp_mad_mean_stat <- function(data, indices){
  data <- data[indices,] # allows boot to select sample 
  mad_mean_control <- aad(data$revenues[data$treatment==0])
  exp_mad_mean_control <- log(mad_mean_control)
  return(exp_mad_mean_control)
}

mad_mean_est <- rep(NA,K)
mad_mean_se <- rep(NA,K)
mad_mean_diff_est <- rep(NA,K)
mad_mean_diff_se <- rep(NA,K)

for(i in 1:K){
  boot_out <- boot(data= data[data$site==i,] , statistic = exp_mad_mean_stat , R= 2000)
  mad_mean_est[i] <- boot_out$t0
  mad_mean_se[i] <- sd(boot_out$t)
  boot_out <- boot(data= data[data$site==i,] , statistic = exp_diff_mad_mean_stat , R= 2000)
  mad_mean_diff_est[i] <- boot_out$t0
  mad_mean_diff_se[i] <- sd(boot_out$t)
}


### COMPUTING MEDIAN ABS DEVIATION FROM MEDIAN ("median_mad_median") ###

median_mad_median_control <- rep(NA, K)
median_mad_median_treatment <- rep(NA, K)
median_mad_median_TE <- rep(NA, K)
exp_median_mad_median_control <- rep(NA, K)
exp_median_mad_median_TE <- rep(NA, K)

for(i in 1:K){
  median_mad_median_control[i] <- mad(data$revenues[data$site==i & data$treatment==0], constant = 1)
  median_mad_median_treatment[i] <- mad(data$revenues[data$site==i & data$treatment==1], constant = 1) 
  median_mad_median_TE[i] <- median_mad_median_treatment[i] - median_mad_median_control[i] 
  exp_median_mad_median_control[i] <- log(median_mad_median_control[i])
  exp_median_mad_median_TE[i] <- log(median_mad_median_treatment[i]) - log(median_mad_median_control[i])
}  

exp_median_mad_median_TE


exp_diff_median_mad_median_stat <- function(data, indices){
  data <- data[indices,] # allows boot to select sample 
  mad_mean_control <- mad(data$revenues[data$treatment==0], constant = 1)
  mad_mean_treatment <- mad(data$revenues[data$treatment==1], constant = 1)
  mad_mean_TE <- mad_mean_treatment - mad_mean_control 
  exp_mad_mean_control <- log(mad_mean_control)
  exp_mad_mean_TE <- log(mad_mean_treatment) - log(mad_mean_control)
  return(exp_mad_mean_TE)
}

exp_median_mad_median_stat <- function(data, indices){
  data <- data[indices,] # allows boot to select sample 
  mad_mean_control <- mad(data$revenues[data$treatment==0], constant = 1)
  exp_mad_mean_control <- log(mad_mean_control)
  return(exp_mad_mean_control)
}


median_mad_median_est <- rep(NA,K)
median_mad_median_se <- rep(NA,K)
median_mad_median_diff_est <- rep(NA,K)
median_mad_median_diff_se <- rep(NA,K)

for(i in 1:K){
  boot_out <- boot(data= data[data$site==i,] , statistic = exp_diff_median_mad_median_stat , R= 2000)
  median_mad_median_diff_est[i] <- boot_out$t0
  median_mad_median_diff_se[i] <- sd(boot_out$t)
  boot_out <- boot(data= data[data$site==i,] , statistic = exp_median_mad_median_stat , R= 2000)
  median_mad_median_est[i] <- boot_out$t0
  median_mad_median_se[i] <- sd(boot_out$t)
  

}


## Now let's get those mus and taus

mu_k <- rep(NA, K)
tau_k <- rep(NA, K)
mu_se_k <- rep(NA, K)
tau_se_k <- rep(NA, K)

for(i in 1:K){
ols_out <- summary(lm(data$revenues[data$site==i] ~ data$treatment[data$site==i]))
mu_k[i] <- ols_out$coef[1,1]
tau_k[i] <- ols_out$coef[2,1]
mu_se_k[i] <- ols_out$coef[1,2]
tau_se_k[i] <- ols_out$coef[2,2]}

### STAN MODEL MACH 1 ###

# data! 
P <- 4

data_for_stan <- list(K = length(unique(data$site)),
                      P = 4,
                      mu_k_hat = mu_k,
                      tau_k_hat = tau_k,
                      se_mu_k = mu_se_k,
                      se_tau_k = tau_se_k,
                      eta_k_hat =  mad_mean_est  ,
                      gamma_k_hat = mad_mean_diff_est ,
                      se_eta_k = mad_mean_se,
                      se_gamma_k = mad_mean_diff_se,
                      theta_prior_sigma = diag(50,P),
                      theta_prior_mean = rep(0,P))

stan_fit <- stan("stan code/generalized-joint-rubin-model-for-non-negative-objects.stan", iter = 5000, cores = 4, data = data_for_stan)

print(stan_fit)

stan_fit_summary <- summary(stan_fit)
stan_fit_table <- xtable(stan_fit_summary$summary)

saveRDS(stan_fit_summary, file = "output/microcredit_MAD_mean_effects_revenues_model.RDS", ascii = FALSE, version = NULL,
        compress = TRUE, refhook = NULL)


save.image("output/microcredit_MAD_mean_effects_revenues.RData")
