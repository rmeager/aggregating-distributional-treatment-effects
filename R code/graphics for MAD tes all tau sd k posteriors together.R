# graphics for all tau_k and tau_np_k together
# Rachael Meager
# First Version: nov 2015

### Notes ###

# Analysis needs to be done before you can run this, obviously

### Preliminaries and Data Intake ###
# clear the workspace to avoid gremlins and past globals from past irresponsible scripts
# but we can't do this if the masterfile is being used to run the script, so we check that first:
if(exists("masterfile_run") == "FALSE"){
  rm(list = ls())
}

installation_needed  <- FALSE
loading_needed <- TRUE
package_list <- c('ggplot2', 'rstan','reshape','reshape2','coda','xtable', 'dplyr', 'Runuran', 'testthat',
                  "MCMCpack", "geoR", "gtools", 'gPdtest', 'fBasics',"PtProcess", "VGAM", "MASS","quantreg",
                  "boot","gridExtra")
if(installation_needed){install.packages(package_list, repos='http://cran.us.r-project.org')}
if(loading_needed){lapply(package_list, require, character.only = TRUE)}

# preliminary structuring variables

study_country <- c("Mexico", "Mongolia", "Bosnia", "India", "Morocco", "Philippines", "Ethiopia")
study_country_truncated <- c("Mexico", "Mongolia", "Bosnia", "India", "Morocco")


tau_k_names <- c("theta_k[1,4]","theta_k[2,4]","theta_k[3,4]","theta_k[4,4]",
                 "theta_k[5,4]","theta_k[6,4]","theta_k[7,4]")

country <- study_country

# Load data
load("output/microcredit_MAD_mean_effects_profit.RData")
textable_profit_variance_joint <- stan_fit_table
textable_profit_variance_independent_no_pooling <- data.frame(mad_mean_diff_est, 
                                                              mad_mean_diff_est - 1.96*mad_mean_diff_se,
                                                              mad_mean_diff_est - 0.67*mad_mean_diff_se,
                                                              mad_mean_diff_est + 0.67*mad_mean_diff_se,
                                                              mad_mean_diff_est + 1.96*mad_mean_diff_se)
colnames(textable_profit_variance_independent_no_pooling) <- c("mean", "X2.5.", "X25." ,"X75.","X97.5.") 
rownames(textable_profit_variance_independent_no_pooling) <- tau_k_names[1:length( mad_mean_diff_est)]

load("output/microcredit_MAD_mean_effects_expenditures.RData")
textable_expenditures_variance_joint <- stan_fit_table
textable_expenditures_variance_independent_no_pooling <- data.frame(mad_mean_diff_est, 
                                                              mad_mean_diff_est - 1.96*mad_mean_diff_se,
                                                              mad_mean_diff_est - 0.67*mad_mean_diff_se,
                                                              mad_mean_diff_est + 0.67*mad_mean_diff_se,
                                                              mad_mean_diff_est + 1.96*mad_mean_diff_se)
colnames(textable_expenditures_variance_independent_no_pooling) <- c("mean", "X2.5.", "X25." ,"X75.","X97.5.")
rownames(textable_expenditures_variance_independent_no_pooling) <- tau_k_names[1:length( mad_mean_diff_est)]

load("output/microcredit_MAD_mean_effects_revenues.RData")
textable_revenues_variance_joint <- stan_fit_table
textable_revenues_variance_independent_no_pooling <- data.frame(mad_mean_diff_est, 
                                                              mad_mean_diff_est - 1.96*mad_mean_diff_se,
                                                              mad_mean_diff_est - 0.67*mad_mean_diff_se,
                                                              mad_mean_diff_est + 0.67*mad_mean_diff_se,
                                                              mad_mean_diff_est + 1.96*mad_mean_diff_se)
colnames(textable_revenues_variance_independent_no_pooling) <- c("mean", "X2.5.", "X25." ,"X75.","X97.5.")
rownames(textable_revenues_variance_independent_no_pooling) <- tau_k_names[1:length( mad_mean_diff_est)]

load("output/microcredit_MAD_mean_effects_consumption.RData")
textable_consumption_variance_joint <- stan_fit_table
textable_consumption_variance_independent_no_pooling <- data.frame(mad_mean_diff_est, 
                                                              mad_mean_diff_est - 1.96*mad_mean_diff_se,
                                                              mad_mean_diff_est - 0.67*mad_mean_diff_se,
                                                              mad_mean_diff_est + 0.67*mad_mean_diff_se,
                                                              mad_mean_diff_est + 1.96*mad_mean_diff_se)
colnames(textable_consumption_variance_independent_no_pooling) <- c("mean", "X2.5.", "X25." ,"X75.","X97.5.")
rownames(textable_consumption_variance_independent_no_pooling) <- tau_k_names[1:length( mad_mean_diff_est)]

load("output/microcredit_MAD_mean_effects_consumerdurables.RData")
textable_consumerdurables_variance_joint <- stan_fit_table
textable_consumerdurables_variance_independent_no_pooling <- data.frame(mad_mean_diff_est, 
                                                              mad_mean_diff_est - 1.96*mad_mean_diff_se,
                                                              mad_mean_diff_est - 0.67*mad_mean_diff_se,
                                                              mad_mean_diff_est + 0.67*mad_mean_diff_se,
                                                              mad_mean_diff_est + 1.96*mad_mean_diff_se)
colnames(textable_consumerdurables_variance_independent_no_pooling) <- c("mean", "X2.5.", "X25." ,"X75.","X97.5.")
rownames(textable_consumerdurables_variance_independent_no_pooling) <- tau_k_names[1:length( mad_mean_diff_est)]

load("output/microcredit_MAD_mean_effects_temptation.RData")
textable_temptation_variance_joint <- stan_fit_table
textable_temptation_variance_independent_no_pooling <- data.frame(mad_mean_diff_est, 
                                                              mad_mean_diff_est - 1.96*mad_mean_diff_se,
                                                              mad_mean_diff_est - 0.67*mad_mean_diff_se,
                                                              mad_mean_diff_est + 0.67*mad_mean_diff_se,
                                                              mad_mean_diff_est + 1.96*mad_mean_diff_se)
colnames(textable_temptation_variance_independent_no_pooling) <- c("mean", "X2.5.", "X25." ,"X75.","X97.5.")
rownames(textable_temptation_variance_independent_no_pooling) <- tau_k_names[1:length( mad_mean_diff_est)]

## JOINT MODEL ###

study_country <- c("Mexico", "Mongolia", "Bosnia", "India", "Morocco", "Philippines", "Ethiopia")
study_country_truncated <- c("Mexico", "Mongolia", "Bosnia", "India", "Morocco")


tau_k_names <- c("theta_k[1,4]","theta_k[2,4]","theta_k[3,4]","theta_k[4,4]",
                 "theta_k[5,4]","theta_k[6,4]","theta_k[7,4]")

country <- study_country



# profit graph
textable_tau_ks <- textable_profit_variance_joint[tau_k_names,]
tau_k_df <- data.frame(country, textable_tau_ks)
textable_tau_np_ks <- textable_profit_variance_independent_no_pooling[tau_k_names,]
tau_np_k_df <- data.frame(country, textable_tau_np_ks)

estimate_type <- c(rep("Bayesian Hierarchical Model",7), rep("No Pooling Model",7))
estimate_country <- rep(country,2)
estimate_means <- c(tau_k_df$mean, tau_np_k_df$mean)
estimate_X2.5. <- c(tau_k_df$X2.5., tau_np_k_df$X2.5.)
estimate_X97.5. <- c(tau_k_df$X97.5., tau_np_k_df$X97.5.)
estimate_X25. <- c(tau_k_df$X25., tau_np_k_df$X25.)
estimate_X75. <- c(tau_k_df$X75., tau_np_k_df$X75.)

estimates_df <- data.frame(estimate_type, estimate_country, estimate_means, estimate_X2.5., estimate_X97.5., estimate_X25., estimate_X75.)

bhm_split_plot <- ggplot(data=estimates_df, aes(x=factor(estimate_country), y=estimate_means, color = factor(estimate_type), fill  = factor(estimate_type), width=.5,  alpha= 0.25)) 

bhm_split_fancy_plot <- bhm_split_plot +  geom_boxplot(aes(ymax=estimate_X97.5., ymin=estimate_X2.5., lower=estimate_X25. , upper = estimate_X75., middle = estimate_means), stat=('identity'))

profit_plot <- bhm_split_fancy_plot + labs(title = "Business Profit")  + coord_flip(ylim=c(-0.25,1.2)) +theme(legend.position="bottom") +ylab("Posterior mean, 50% interval (box), and 95% interval (line)\n for each Treatment Effect (multiplicative exponentiated)") + xlab("") + theme(legend.title=element_blank()) + scale_alpha_identity()+ theme(text = element_text(size=10,color="black"), axis.text.y = element_text(size=10,color="grey40"),axis.text.x = element_text(size=10,color="grey40"))

pdf(file="output/MAD_effects_k_posteriors_profit.pdf",width = 5, height = 5)
profit_plot
dev.off()

# revenues graph
textable_tau_ks <- textable_revenues_variance_joint[tau_k_names,]
tau_k_df <- data.frame(country, textable_tau_ks)
textable_tau_np_ks <- textable_revenues_variance_independent_no_pooling[tau_k_names,]
tau_np_k_df <- data.frame(country, textable_tau_np_ks)

estimate_type <- c(rep("Bayesian Hierarchical Model",7), rep("No Pooling Model",7))
estimate_country <- rep(country,2)
estimate_means <- c(tau_k_df$mean, tau_np_k_df$mean)
estimate_X2.5. <- c(tau_k_df$X2.5., tau_np_k_df$X2.5.)
estimate_X97.5. <- c(tau_k_df$X97.5., tau_np_k_df$X97.5.)
estimate_X25. <- c(tau_k_df$X25., tau_np_k_df$X25.)
estimate_X75. <- c(tau_k_df$X75., tau_np_k_df$X75.)

estimates_df <- data.frame(estimate_type, estimate_country, estimate_means, estimate_X2.5., estimate_X97.5., estimate_X25., estimate_X75.)

bhm_split_plot <- ggplot(data=estimates_df, aes(x=factor(estimate_country), y=estimate_means, color = factor(estimate_type), fill  = factor(estimate_type), width=.5,  alpha= 0.25)) 

bhm_split_fancy_plot <- bhm_split_plot +  geom_boxplot(aes(ymax=estimate_X97.5., ymin=estimate_X2.5., lower=estimate_X25. , upper = estimate_X75., middle = estimate_means), stat=('identity'))

revenues_plot <- bhm_split_fancy_plot + labs(title = "Business Revenues")  + coord_flip(ylim=c(-0.5,1.5)) +theme(legend.position="bottom") +ylab("Posterior mean, 50% interval (box), and 95% interval (line)\n for each Treatment Effect (multiplicative exponentiated)") + xlab("") + theme(legend.title=element_blank()) + scale_alpha_identity()+ theme(text = element_text(size=10,color="black"), axis.text.y = element_text(size=10,color="grey40"),axis.text.x = element_text(size=10,color="grey40"))


# expenditures graph
textable_tau_ks <- textable_expenditures_variance_joint[tau_k_names,]
tau_k_df <- data.frame(country, textable_tau_ks)
textable_tau_np_ks <- textable_expenditures_variance_independent_no_pooling[tau_k_names,]
tau_np_k_df <- data.frame(country, textable_tau_np_ks)

estimate_type <- c(rep("Bayesian Hierarchical Model",7), rep("No Pooling Model",7))
estimate_country <- rep(country,2)
estimate_means <- c(tau_k_df$mean, tau_np_k_df$mean)
estimate_X2.5. <- c(tau_k_df$X2.5., tau_np_k_df$X2.5.)
estimate_X97.5. <- c(tau_k_df$X97.5., tau_np_k_df$X97.5.)
estimate_X25. <- c(tau_k_df$X25., tau_np_k_df$X25.)
estimate_X75. <- c(tau_k_df$X75., tau_np_k_df$X75.)

estimates_df <- data.frame(estimate_type, estimate_country, estimate_means, estimate_X2.5., estimate_X97.5., estimate_X25., estimate_X75.)

bhm_split_plot <- ggplot(data=estimates_df, aes(x=factor(estimate_country), y=estimate_means, color = factor(estimate_type), fill  = factor(estimate_type), width=.5,  alpha= 0.25)) 

bhm_split_fancy_plot <- bhm_split_plot +  geom_boxplot(aes(ymax=estimate_X97.5., ymin=estimate_X2.5., lower=estimate_X25. , upper = estimate_X75., middle = estimate_means), stat=('identity'))

expenditures_plot <- bhm_split_fancy_plot + labs(title = "Business Expenditures")  + coord_flip(ylim=c(-0.5,1.5)) +theme(legend.position="bottom") +ylab("Posterior mean, 50% interval (box), and 95% interval (line)\n for each Treatment Effect (multiplicative exponentiated)") + xlab("") + theme(legend.title=element_blank()) + scale_alpha_identity()+ theme(text = element_text(size=10,color="black"), axis.text.y = element_text(size=10,color="grey40"),axis.text.x = element_text(size=10,color="grey40"))


# consumption graph
tau_k_names <- tau_k_names[1:5]
country <- country[1:5]
textable_tau_ks <- textable_consumption_variance_joint[tau_k_names,]
tau_k_df <- data.frame(country, textable_tau_ks)
textable_tau_np_ks <- textable_consumption_variance_independent_no_pooling[tau_k_names,]
tau_np_k_df <- data.frame(country, textable_tau_np_ks)

estimate_type <- c(rep("Bayesian Hierarchical Model",5), rep("No Pooling Model",5))
estimate_country <- rep(country,2)
estimate_means <- c(tau_k_df$mean, tau_np_k_df$mean)
estimate_X2.5. <- c(tau_k_df$X2.5., tau_np_k_df$X2.5.)
estimate_X97.5. <- c(tau_k_df$X97.5., tau_np_k_df$X97.5.)
estimate_X25. <- c(tau_k_df$X25., tau_np_k_df$X25.)
estimate_X75. <- c(tau_k_df$X75., tau_np_k_df$X75.)

estimates_df <- data.frame(estimate_type, estimate_country, estimate_means, estimate_X2.5., estimate_X97.5., estimate_X25., estimate_X75.)

bhm_split_plot <- ggplot(data=estimates_df, aes(x=factor(estimate_country), y=estimate_means, color = factor(estimate_type), fill  = factor(estimate_type), width=.5,  alpha= 0.25)) 

bhm_split_fancy_plot <- bhm_split_plot +  geom_boxplot(aes(ymax=estimate_X97.5., ymin=estimate_X2.5., lower=estimate_X25. , upper = estimate_X75., middle = estimate_means), stat=('identity'))

consumption_plot <- bhm_split_fancy_plot + labs(title = "Consumption")  + coord_flip(ylim=c(-0.25,0.75)) +theme(legend.position="bottom") +ylab("Posterior mean, 50% interval (box), and 95% interval (line)\n for each Treatment Effect (multiplicative exponentiated)") + xlab("") + theme(legend.title=element_blank()) + scale_alpha_identity()+ theme(text = element_text(size=10,color="black"), axis.text.y = element_text(size=10,color="grey40"),axis.text.x = element_text(size=10,color="grey40"))

pdf(file="output/MAD_effects_k_posteriors_consumption.pdf",width = 5, height = 5)
consumption_plot
dev.off()
# consumerdurables graph
tau_k_names <- tau_k_names[1:4]
country <- study_country[2:5]
textable_tau_ks <- textable_consumerdurables_variance_joint[tau_k_names,]
tau_k_df <- data.frame(country, textable_tau_ks)
textable_tau_np_ks <- textable_consumerdurables_variance_independent_no_pooling[tau_k_names,]
tau_np_k_df <- data.frame(country, textable_tau_np_ks)

estimate_type <- c(rep("Bayesian Hierarchical Model",4), rep("No Pooling Model",4))
estimate_country <- rep(country,2)
estimate_means <- c(tau_k_df$mean, tau_np_k_df$mean)
estimate_X2.5. <- c(tau_k_df$X2.5., tau_np_k_df$X2.5.)
estimate_X97.5. <- c(tau_k_df$X97.5., tau_np_k_df$X97.5.)
estimate_X25. <- c(tau_k_df$X25., tau_np_k_df$X25.)
estimate_X75. <- c(tau_k_df$X75., tau_np_k_df$X75.)

estimates_df <- data.frame(estimate_type, estimate_country, estimate_means, estimate_X2.5., estimate_X97.5., estimate_X25., estimate_X75.)

bhm_split_plot <- ggplot(data=estimates_df, aes(x=factor(estimate_country), y=estimate_means, color = factor(estimate_type), fill  = factor(estimate_type), width=.5,  alpha= 0.25)) 

bhm_split_fancy_plot <- bhm_split_plot +  geom_boxplot(aes(ymax=estimate_X97.5., ymin=estimate_X2.5., lower=estimate_X25. , upper = estimate_X75., middle = estimate_means), stat=('identity'))

consumerdurables_plot <- bhm_split_fancy_plot + labs(title = "Consumer durables spending")  + coord_flip(ylim=c(-0.5,1)) +theme(legend.position="bottom") +ylab("Posterior mean, 50% interval (box), and 95% interval (line)\n for each Treatment Effect (multiplicative exponentiated)") + xlab("") + theme(legend.title=element_blank()) + scale_alpha_identity()+ theme(text = element_text(size=10,color="black"), axis.text.y = element_text(size=10,color="grey40"),axis.text.x = element_text(size=10,color="grey40"))

# temptation graph
tau_k_names <- tau_k_names[1:5]
country <- study_country[1:5]
textable_tau_ks <- textable_temptation_variance_joint[tau_k_names,]
tau_k_df <- data.frame(country, textable_tau_ks)
textable_tau_np_ks <- textable_temptation_variance_independent_no_pooling[tau_k_names,]
tau_np_k_df <- data.frame(country, textable_tau_np_ks)

estimate_type <- c(rep("Bayesian Hierarchical Model",5), rep("No Pooling Model",5))
estimate_country <- rep(country,2)
estimate_means <- c(tau_k_df$mean, tau_np_k_df$mean)
estimate_X2.5. <- c(tau_k_df$X2.5., tau_np_k_df$X2.5.)
estimate_X97.5. <- c(tau_k_df$X97.5., tau_np_k_df$X97.5.)
estimate_X25. <- c(tau_k_df$X25., tau_np_k_df$X25.)
estimate_X75. <- c(tau_k_df$X75., tau_np_k_df$X75.)

estimates_df <- data.frame(estimate_type, estimate_country, estimate_means, estimate_X2.5., estimate_X97.5., estimate_X25., estimate_X75.)

bhm_split_plot <- ggplot(data=estimates_df, aes(x=factor(estimate_country), y=estimate_means, color = factor(estimate_type), fill  = factor(estimate_type), width=.5,  alpha= 0.25)) 

bhm_split_fancy_plot <- bhm_split_plot +  geom_boxplot(aes(ymax=estimate_X97.5., ymin=estimate_X2.5., lower=estimate_X25. , upper = estimate_X75., middle = estimate_means), stat=('identity'))

temptation_plot <- bhm_split_fancy_plot + labs(title = "Temptation goods spending")  + coord_flip(ylim=c(-0.5,1)) +theme(legend.position="bottom") +ylab("Posterior mean, 50% interval (box), and 95% interval (line)\n for each Treatment Effect (multiplicative exponentiated)") + xlab("") + theme(legend.title=element_blank()) + scale_alpha_identity()+ theme(text = element_text(size=10,color="black"), axis.text.y = element_text(size=10,color="grey40"),axis.text.x = element_text(size=10,color="grey40"))



tauk_plotlist <- list( profit_plot,
                       revenues_plot,
                       expenditures_plot,
                       consumption_plot,
                       consumerdurables_plot,
                       temptation_plot)

pdf(file="output/MAD_effects_k_posteriors_all_together.pdf",width = 9, height = 11)
grid.arrange(profit_plot,
             revenues_plot,
             expenditures_plot,
             consumption_plot,
             consumerdurables_plot,
             temptation_plot, ncol=2, top = "Joint Model")
dev.off()

