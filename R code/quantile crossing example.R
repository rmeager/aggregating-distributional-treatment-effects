# multivariate normal aggregation without constraints: quantile crossing example
# Rachael Meager
# April 2016 

### Notes

### Preliminaries 

# clear the workspace to avoid gremlins and past globals from past irresponsible scripts
# but we can't do this if the masterfile is being used to run the script, so we check that first:
if(exists("masterfile_run") == "FALSE"){
  rm(list = ls())
}

# install and load packages


installation_needed  <- FALSE
loading_needed <- TRUE
package_list <- c('ggplot2', 'rstan','reshape','reshape2','coda','xtable', 'dplyr', 'Runuran', 'testthat',
                  "MCMCpack", "geoR", "gtools", 'gPdtest', 'fBasics',"PtProcess", "VGAM", "MASS","quantreg",
                  "boot")
if(installation_needed){install.packages(package_list, repos='http://cran.us.r-project.org')}
if(loading_needed){lapply(package_list, require, character.only = TRUE)}


# simulate some fake data

K <- 2
N <- 10
quantiles_list <- seq(0.05, 0.95, 0.1)
y_0_1 <- c(6,12.7,14,21,24,25,25,25.5,25.7,25.9)
y_0_2 <- c(6,6.5,7,8,9,10.5,10.6,10.7,10.8,11)
y_0_se_1 <- c(2, 1.2, 1 , 0.4, 0.2 ,0.5 ,3 ,4 ,6 ,8) 
y_0_se_2 <- c(7 ,6 ,5 , 5.3 ,6 ,4 ,1 , 0.6,0.6 ,0.5 )
y_0 <- rbind(y_0_1, y_0_2)
y_0_se <- rbind(y_0_se_1, y_0_se_2)

# now STAN MODEL! 
# fit the model 
fit_data <- list(N = N,
                 K = K,
                 theta = quantiles_list,
                 y_0 = y_0,
                 se_y_k_0 = y_0_se,
                 prior_dispersion_on_beta_0 = 1000*diag(N))

stan_fit <- stan("stan code/hierarchical-multivariate-normal-basic-quantiles.stan",
                 iter = 5000, cores = 4, data = fit_data, control = list(adapt_delta=0.9, max_treedepth = 12))

print(stan_fit)

stan_fit_summary <- summary(stan_fit)
stan_fit_table <- xtable(stan_fit_summary$summary)
save.image("output/multivariate_normal_quantile_example.RData")



### now for the graphics ###

# make ribbon plot for posterior 
posterior_beta_data <- stan_fit_table[grep("beta_0", rownames(stan_fit_table)),]
posterior_beta_data <- posterior_beta_data[-grep("beta_0_k", rownames(posterior_beta_data)),]
posterior_beta_data <- cbind(posterior_beta_data, quantiles_list)

posterior_beta_data_plot <- ggplot(posterior_beta_data, aes(posterior_beta_data$quantiles_list))
pdf("output/posterior_quantiles_crossing_example.pdf", width=5, height=6)
posterior_beta_data_plot +
  geom_ribbon(aes(ymin = posterior_beta_data[,"2.5%"], ymax = posterior_beta_data[,"97.5%"]), fill = "purple", alpha=0.3) +
  geom_ribbon(aes(ymin = posterior_beta_data[,"25%"], ymax = posterior_beta_data[,"75%"]), fill = "purple", alpha=0.6) +
  geom_line(aes(y = posterior_beta_data[,"mean"]), color = "black", size = 1.5) +
  ggtitle("Posterior Aggregate Curve") +
  theme(plot.title = element_text(size = 14)) + xlim(0.05,0.95) +  ylim(-22,42) +
  xlab("Quantiles") + ylab("Value")+
  theme(axis.text = element_text(size=14)) +  theme(axis.title.y = element_text(size = 14)) +  theme(axis.title.x = element_text(size = 14))
dev.off()


# make component graphs 

y_0_1_data <- data.frame(y_0_1, 
                         y_0_se_1,
                         y_0_1 - 1.96*y_0_se_1, 
                         y_0_1 - 0.67*y_0_se_1,
                         y_0_1 + 0.67*y_0_se_1,
                         y_0_1 + 1.96*y_0_se_1,
                         quantiles_list)

colnames(y_0_1_data) <- c("mean", "sd","2.5%", "25%","75%", "97.5%","quantiles")

y_0_2_data <- data.frame(y_0_2, 
                         y_0_se_1,
                         y_0_2 - 1.96*y_0_se_2, 
                         y_0_2 - 0.67*y_0_se_2,
                         y_0_2 + 0.67*y_0_se_2,
                         y_0_2 + 1.96*y_0_se_2,
                         quantiles_list)

colnames(y_0_2_data) <- c("mean", "sd","2.5%", "25%","75%", "97.5%","quantiles")

names_repeated <- c(rep("curve 1",10),rep("curve 2",10))

posterior_k_beta_data <- rbind(y_0_1_data, y_0_2_data)

posterior_k_beta_data <- cbind(posterior_k_beta_data, names_repeated)

posterior_k_beta_data_plot <- ggplot(posterior_k_beta_data, aes(x = quantiles, y = mean))
pdf("output/original_curves_example.pdf", width=5, height=6)
posterior_k_beta_data_plot +
  geom_ribbon(aes(ymin = posterior_k_beta_data[,"2.5%"], ymax =posterior_k_beta_data[,"97.5%"]), fill = "blue", alpha=0.3) +
  geom_ribbon(aes(ymin = posterior_k_beta_data[,"25%"], ymax = posterior_k_beta_data[,"75%"]), fill = "blue", alpha=0.6) +
  geom_line(aes(y = posterior_k_beta_data[,"mean"]),color = "black", size = 1) +
  facet_grid(names_repeated~.) +
  ggtitle("Original Curves") +
  theme(plot.title = element_text(size = 14)) + xlim(0.05,0.95)  +
  xlab("Quantiles") + ylab("Value")+
  theme(axis.text = element_text(size=14)) +  theme(axis.title.y = element_text(size = 14)) +  theme(axis.title.x = element_text(size = 14))
dev.off()
