README FOR THE DIRECTORY ASSOCIATED WITH THE PROJECT "AGGREGATING DISTRIBUTIONAL TREATMENT EFFECTS: A BAYESIAN HIERARCHICAL APPROACH TO THE MICROCREDIT LITERATURE" 
Author: Rachael Meager (rachael.meager@gmail.com)
Initial Date: 11/29/2016
Most Recent Update: 06/05/2020

0. THIS DIRECTORY IS IN THE PROCESS OF BEING UPDATED WITH THE NEW CODE WRITTEN AT THE REQUEST OF SEVERAL REFEREES. PLEASE CHECK BACK FOR UPDATES IN THE NEAR FUTURE. 

1.  This directory contains all the code ever written for the project above, taking the cleaned data as given. 
It is not an example of the highest standards of project management but it does work and hopefully makes sense to the average user. 
To reproduce the paper in full: (1) set the directory to your local directory, (2) first run the R scripts that do not contain the word “graphics” in the file name, then run the R scripts that do contain the word “graphics” in the file name, (3) compile the most recent version of the latex document in the output folder. 

2. (a) The scripts call for installation of certain packages: the R code has a toggle for installation which is set to OFF in all scripts by default. (In an older version of this repo there was a masterfile.R that used to do installation, but the new version of the project can’t accommodate that for dumb reasons I will explain below in bullet point 7.)
To switch it, go to any script and flip the toggle in the very first line of code (installation_needed <- FALSE). 
The toggle is present in every script and only needs to be switched on in the first script you run. 

2. (b) This project critically relies on the package Rstan, which only works if your machine has a C++ compiler that can talk to R.  
If you don't have one, you can find instructions for downloading and setting up Rstan and the requisite compiler here: https://github.com/stan-dev/rstan/wiki/RStan-Getting-Started.
Configuration is described as "optional but highly recommended", I have found it to be non-optional for my machines. 

3. Each script is a full code module which can be run separately and they are designed to be self-contained, with the exception of any script that starts with the words "graphics" or "ridge".
 (Obviously you can't make graphics of output which doesn't exist yet). 

4.  The code in this directory reproduces every figure and table shown in the paper and appendices uploaded to the OSF website. 
However, the table formatting will be different because of some compatibility problems I had with latex and R. 
Some of the paper's tables were manually copied into the latex file and modified to improve their formatting. 
The raw tables are produced as .txt or .tex files in the output directory.

5. The output files produced by this project are very large. 
Stanfit objects are large objects and this project uses many, many stanfit objects which are saved to the output folder (this was largely to give me flexibility when I wrote the graphics and second-stage analysis files). 
You will need a decent amount of free storage space if you want to replicate this project (on the order of 30 GB). 
R is also RAM-intensive, and I use remote servers with a lot of RAM to run this project. 
If the project isn't working for you, check these issues before you email me. 
R sometimes issues generic errors in these cases, so the error message may not point you to them.

6. A note about the data: The input data file I provide in this directory was compiled from the 7 data sets available online and then cleaned and standardized. 
The procedure for this compilation is messy and requires some cleaning in Stata (using the original code from the authors, to ensure compliance with their results) and some cleaning in R. 
Because I want the project to be fully replicable with free and open source software, I have omitted the Stata-dependent components of the project and supplied this directory with the cleaned data.  
That means some of the comments and stops in the code, which were designed to prevent the loading of the wrong data files (e.g. after discovering errors in the cleaning code), are not relevant to you. 
In general you may disregard comments relating to importing raw data. 

If you want to see how the data was cleaned and standardised, the original data files and all associated scripts are in the folder “microcredit rct data”.

7. There are two Stan code folders because when I graduated from MIT and moved to LSE, I had to use a new high performance computing cluster and this new server did not permit directories with spaces in them. This is why the masterfile which I used to have in this repo that ran the whole project no longer works, and has been deleted. The moral of the story is either never put spaces in file names, or never graduate from your PhD, I am not sure which.


8. If you see any errors in this code please alert me immediately via email. 
If the code does not run on your machine and delivers generic errors, please check package installation issues, test Stan as described in step 2, and check RAM and storage before contacting me. 