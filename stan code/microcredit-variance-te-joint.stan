
functions {
  real normal_ss_log(int N_c, int N_t, real y_control_sq_sum, real y_treatment_sq_sum, real y_control_sum, real y_treatment_sum, real mu_k, real tau_k, real sigma_control_k, real sigma_treatment_k) {

    real lp;
    lp <- -.5*((y_control_sq_sum - 2*mu_k*y_control_sum + N_c*mu_k^2)/sigma_control_k^2) - .5*N_c*log(sigma_control_k^2);
    lp <- lp -.5*((y_treatment_sq_sum - 2*(mu_k + tau_k)*y_treatment_sum + N_t*(mu_k + tau_k)^2)/sigma_treatment_k^2) - .5*N_t*log(sigma_treatment_k^2);
    return lp;
  }
}


data {
int N ;// number of observations
int K ;// number of sites
real y[N];// outcome variables of interest
int ITT[N];// intention to treat indicator
int site[N]; // site counter or indicator 
}


transformed data {
  int N_c_k[K];  // number of observations in control from site K
  int N_t_k[K];  // number of observations in treatment from site K
  real y_control_sq_sum[K];  // 
  real y_treatment_sq_sum[K];  //
  real y_control_sum[K];  //
  real y_treatment_sum[K];  //
  int s;

  // initialize everything to zero
  N_c_k <- rep_array(0, K);
  N_t_k <- rep_array(0, K);
  y_control_sq_sum <- rep_array(0.0, K);
  y_treatment_sq_sum <- rep_array(0.0, K);
  y_control_sum <- rep_array(0.0, K);
  y_treatment_sum <- rep_array(0.0, K);
  
  for (n in 1:N) {
    s <- site[n];
    N_c_k[s] <- N_c_k[s] + (1-ITT[n]);
    N_t_k[s] <- N_t_k[s] + (ITT[n]);
    y_control_sq_sum[s] <- y_control_sq_sum[s] + (1-ITT[n])*(y[n]^2);
    y_treatment_sq_sum[s] <- y_treatment_sq_sum[s] + ITT[n]*(y[n]^2);
    y_control_sum[s] <- y_control_sum[s] + (1-ITT[n])*(y[n]);
    y_treatment_sum[s] <- y_treatment_sum[s] + ITT[n]*(y[n]);
  
  }
}



parameters {
real mu;// mean
real tau ;// mean TE
real tausigma ;// sd TE
real musigma ;// sd TE

real mu_k[K];// mean
real tau_k[K] ;// mean TE
real musigma_k[K] ;// standard deviation of control group on log scale
real tausigma_k[K] ;// sd TE on log scale

cholesky_factor_corr[4] L_Omega; // cholesky factor of correlation matrix 
vector<lower=0>[4] theta; // scale

}
transformed parameters {


real<lower=0> sigma_treatment_k[K] ;// standard deviation total for the treated in each site
real<lower=0> sigma_control_k[K] ;// standard deviation total for the control in each site
cov_matrix[4] parent_Sigma;
vector[4] parent_means;
vector[4] site_means[K];

parent_Sigma <- diag_pre_multiply(theta,L_Omega) * diag_pre_multiply(theta,L_Omega)' ;

parent_means[1] <- mu;
parent_means[2] <- tau;
parent_means[3] <- musigma;
parent_means[4] <- tausigma;

for (k in 1:K) {
sigma_control_k[k] = exp(musigma_k[k]);
sigma_treatment_k[k] = exp(musigma_k[k]+tausigma_k[k]);
site_means[k,1] = mu_k[k];
site_means[k,2] = tau_k[k];
site_means[k,3] = musigma_k[k];
site_means[k,4] = tausigma_k[k];

}

}
model {

mu ~ normal(0,1000);
tau ~ normal(0,1000);
tausigma ~ normal(0,100);
musigma ~ normal(0,100);

theta[1] ~ cauchy(0,50);
theta[2] ~ cauchy(0,50);
theta[3] ~ cauchy(0,5);
theta[4] ~ cauchy(0,5);

L_Omega ~ lkj_corr_cholesky(3);

  for (k in 1:K) {
    site_means[k] ~ multi_normal(parent_means, parent_Sigma);
    
    // real normal_ss_log(int N_c, int N_t, real y_control_sq_sum, real y_treatment_sq_sum, real y_control_sum, real y_treatment_sum, real mu_k, real tau_k, real sigma_control_k, real sigma_treatment_k)

    increment_log_prob(normal_ss_log(N_c_k[k], N_t_k[k], y_control_sq_sum[k], y_treatment_sq_sum[k], y_control_sum[k], y_treatment_sum[k], mu_k[k], tau_k[k], sigma_control_k[k], sigma_treatment_k[k]));
  }

}
generated quantities{
  vector[4] predicted_site_effects_k;
  real predicted_y_treatment;
  real predicted_y_control;

  predicted_site_effects_k = multi_normal_rng(parent_means, parent_Sigma);
  predicted_y_treatment = normal_rng( predicted_site_effects_k[1] + predicted_site_effects_k[2],  exp(predicted_site_effects_k[3] + predicted_site_effects_k[4])  );
  predicted_y_control = normal_rng( predicted_site_effects_k[1],  exp(predicted_site_effects_k[3]) );

}


