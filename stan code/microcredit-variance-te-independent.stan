
functions {
  real normal_ss_log(int N_c, int N_t, real y_control_sq_sum, real y_treatment_sq_sum, real y_control_sum, real y_treatment_sum, real mu_k, real tau_k, real sigma_control_k, real sigma_treatment_k) {

    real lp;
    lp <- -.5*((y_control_sq_sum - 2*mu_k*y_control_sum + N_c*mu_k^2)/sigma_control_k^2) - .5*N_c*log(sigma_control_k^2);
    lp <- lp -.5*((y_treatment_sq_sum - 2*(mu_k + tau_k)*y_treatment_sum + N_t*(mu_k + tau_k)^2)/sigma_treatment_k^2) - .5*N_t*log(sigma_treatment_k^2);
    return lp;
  }
}


data {
int N ;// number of observations
int K ;// number of sites
real y[N];// outcome variables of interest
int ITT[N];// intention to treat indicator
int site[N]; // site counter or indicator 
}


transformed data {
  int N_c_k[K];  // number of observations in control from site K
  int N_t_k[K];  // number of observations in treatment from site K
  real y_control_sq_sum[K];  // 
  real y_treatment_sq_sum[K];  //
  real y_control_sum[K];  //
  real y_treatment_sum[K];  //
  int s;

  // initialize everything to zero
  N_c_k <- rep_array(0, K);
  N_t_k <- rep_array(0, K);
  y_control_sq_sum <- rep_array(0.0, K);
  y_treatment_sq_sum <- rep_array(0.0, K);
  y_control_sum <- rep_array(0.0, K);
  y_treatment_sum <- rep_array(0.0, K);
  
  for (n in 1:N) {
    s <- site[n];
    N_c_k[s] <- N_c_k[s] + (1-ITT[n]);
    N_t_k[s] <- N_t_k[s] + (ITT[n]);
    y_control_sq_sum[s] <- y_control_sq_sum[s] + (1-ITT[n])*(y[n]^2);
    y_treatment_sq_sum[s] <- y_treatment_sq_sum[s] + ITT[n]*(y[n]^2);
    y_control_sum[s] <- y_control_sum[s] + (1-ITT[n])*(y[n]);
    y_treatment_sum[s] <- y_treatment_sum[s] + ITT[n]*(y[n]);
  
  }
}



parameters {
real mu;// mean
real tau ;// mean TE
real tausigma ;// sd TE
real musigma ;// sd TE

real mu_k[K];// mean
real tau_k[K] ;// mean TE
real musigma_k[K] ;// standard deviation of control group on log scale
real tausigma_k[K] ;// sd TE on log scale

real<lower=0> sigma_tau ;// parent sd tau
real<lower=0> sigma_mu ;// parent sd mu
real<lower=0> sigma_tausigma ;// parent sd tausigma
real<lower=0> sigma_musigma ;// parent sd tausigma

}
transformed parameters {


real<lower=0> sigma_treatment_k[K] ;// standard deviation total for the treated in each site
real<lower=0> sigma_control_k[K] ;// standard deviation total for the control in each site


for (k in 1:K) {
sigma_control_k[k] = exp(musigma_k[k]);
sigma_treatment_k[k] = exp(musigma_k[k]+tausigma_k[k]);
}

}
model {

mu ~ normal(0,1000);
tau ~ normal(0,1000);
tausigma ~ normal(0,100);
musigma ~ normal(0,100);

sigma_tau ~ cauchy(0,50);
sigma_mu ~ cauchy(0,50);
sigma_tausigma ~ cauchy(0,5);
sigma_musigma ~ cauchy(0,5);



  for (k in 1:K) {
    mu_k[k] ~ normal(mu, sigma_mu);
    tau_k[k] ~ normal(tau, sigma_tau);
    tausigma_k[k] ~ normal(tausigma, sigma_tausigma);
    musigma_k[k] ~ normal(musigma, sigma_musigma);
    
    // real normal_ss_log(int N_c, int N_t, real y_control_sq_sum, real y_treatment_sq_sum, real y_control_sum, real y_treatment_sum, real mu_k, real tau_k, real sigma_control_k, real sigma_treatment_k)

    increment_log_prob(normal_ss_log(N_c_k[k], N_t_k[k], y_control_sq_sum[k], y_treatment_sq_sum[k], y_control_sum[k], y_treatment_sum[k], mu_k[k], tau_k[k], sigma_control_k[k], sigma_treatment_k[k]));
  }

}
generated quantities{
  real predicted_mu_k;
  real predicted_tau_k;
  real predicted_tausigma_k;
  real predicted_musigma_k;
  real predicted_y_treatment;
  real predicted_y_control;

  predicted_mu_k = normal_rng(mu, sigma_mu);
  predicted_tau_k = normal_rng(tau, sigma_tau);
  predicted_tausigma_k = normal_rng(tausigma, sigma_tausigma);
  predicted_musigma_k = normal_rng(musigma, sigma_musigma);
  predicted_y_treatment = normal_rng( predicted_mu_k + predicted_tau_k,  exp(predicted_musigma_k + predicted_tausigma_k)  );
  predicted_y_control = normal_rng( predicted_mu_k,  exp(predicted_musigma_k) );

}


