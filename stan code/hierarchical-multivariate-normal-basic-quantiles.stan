data {
  int<lower=1> N; // number of quantiles
  int<lower=1> K; // number of sites
  ordered[N] theta; // quantile
  vector[N] y_0[K]; // quantile values 
  vector[N] y_0_se[K]; // quantile standard errors
  cov_matrix[N] prior_dispersion_on_beta_0; // prior dispersion on beta_0
}
transformed data{
  vector[N] density_eval_k_0[K];
  cov_matrix[N] Sigma_y_k_0[K];


  for(k in 1:K){
    // diagonal elements 0
    for (i in 1:N){
      density_eval_k_0[k,i] <- sqrt(theta[i]*(1-theta[i])/pow(y_0_se[k,i],2));
      Sigma_y_k_0[k,i,i] <- pow(y_0_se[k,i],2);
    }
    // off-diagonal elements 0
    for (i in 1:(N-1)) {
      for (j in (i+1):N) {
       Sigma_y_k_0[k,i,j] <- theta[i]*(1-theta[j])/(density_eval_k_0[k,i]*density_eval_k_0[k,j]) ;
        Sigma_y_k_0[k,j,i] <- Sigma_y_k_0[k,i,j];
     }
   }

  } // closes for-loop indexed by k
}
parameters {
  vector[N] beta_0;
  vector[N]  beta_0_k[K];
  cholesky_factor_corr[N] L_Omega_0; // cholesky factor of correlation matrix 
  vector<lower=0>[N] tau_0; // scale


}
transformed parameters {
  cov_matrix[N] Sigma_0;
  Sigma_0 <- diag_pre_multiply(tau_0,L_Omega_0) * diag_pre_multiply(tau_0,L_Omega_0)' ;


}
model {
  tau_0 ~ cauchy(0,20);
  L_Omega_0 ~ lkj_corr_cholesky(1);
  beta_0 ~ multi_normal(rep_vector(0, N), prior_dispersion_on_beta_0);
  for(k in 1:K){
    beta_0_k[k] ~ multi_normal(beta_0, Sigma_0);
   y_0[k] ~ multi_normal(beta_0_k[k], Sigma_y_k_0[k]);
}
}

