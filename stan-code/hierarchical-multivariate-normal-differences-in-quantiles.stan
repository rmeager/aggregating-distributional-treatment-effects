data {
  int<lower=1> N; // number of quantiles
  int<lower=1> K; // number of sites
  ordered[N] theta; // quantile
  vector[N] y_0[K]; // quantile values 
  vector[N] y_1[K]; // quantile values 
  vector[N] y_0_se[K]; // quantile standard errors
  vector[N] y_1_se[K]; // quantile standard errors
  cov_matrix[N] prior_dispersion_on_beta_0; // prior dispersion on beta_0
  cov_matrix[N] prior_dispersion_on_beta_1; // prior dispersion on beta_1
}
transformed data{
  vector[N] density_eval_k_0[K];
  cov_matrix[N] Sigma_y_k_0[K];
  vector[N] density_eval_k_1[K];
  cov_matrix[N] Sigma_y_k_1[K];

  for(k in 1:K){
    // diagonal elements 0
    for (i in 1:N){
      density_eval_k_0[k,i] <- sqrt(theta[i]*(1-theta[i])/pow(y_0_se[k,i],2));
      Sigma_y_k_0[k,i,i] <- pow(y_0_se[k,i],2);
    }
    // off-diagonal elements 0
    for (i in 1:(N-1)) {
      for (j in (i+1):N) {
       Sigma_y_k_0[k,i,j] <- theta[i]*(1-theta[j])/(density_eval_k_0[k,i]*density_eval_k_0[k,j]) ;
        Sigma_y_k_0[k,j,i] <- Sigma_y_k_0[k,i,j];
     }
   }

    // diagonal elements 1
    for (i in 1:N){
      density_eval_k_1[k,i] <- sqrt(theta[i]*(1-theta[i])/pow(y_1_se[k,i],2));
      Sigma_y_k_1[k,i,i] <- pow(y_1_se[k,i],2);
    }
    // off-diagonal elements 1
    for (i in 1:(N-1)) {
      for (j in (i+1):N) {
       Sigma_y_k_1[k,i,j] <- theta[i]*(1-theta[j])/(density_eval_k_1[k,i]*density_eval_k_1[k,j]) ;
        Sigma_y_k_1[k,j,i] <- Sigma_y_k_1[k,i,j];
     }
   }

  } // closes for-loop indexed by k
}
parameters {
  positive_ordered[N] beta_0;
  positive_ordered[N]  beta_0_k[K];
  cholesky_factor_corr[N] L_Omega_0; // cholesky factor of correlation matrix 
  vector<lower=0>[N] tau_0; // scale
  vector[N] beta_1; // true parent DIFFERENCE in treatment and control quantiles
  positive_ordered[N]  treatment_quantiles_k[K]; // true treatment quantiles
  cholesky_factor_corr[N] L_Omega_1; // cholesky factor of correlation matrix 
  vector<lower=0>[N] tau_1; // scale

}
transformed parameters {
  cov_matrix[N] Sigma_0;
  cov_matrix[N] Sigma_1;
  vector[N] beta_1_k[K]; // intermediate treatment EFFECT draws 

  Sigma_0 <- diag_pre_multiply(tau_0,L_Omega_0) * diag_pre_multiply(tau_0,L_Omega_0)' ;
  Sigma_1 <- diag_pre_multiply(tau_1,L_Omega_1) * diag_pre_multiply(tau_1,L_Omega_1)' ;
  for(k in 1:K){
   beta_1_k[k] <- treatment_quantiles_k[k] - beta_0_k[k]; // 
  }

}
model {
  tau_0 ~ cauchy(0,20);
  L_Omega_0 ~ lkj_corr_cholesky(1);
  beta_0 ~ multi_normal(rep_vector(0, N), prior_dispersion_on_beta_0);
  tau_1 ~ cauchy(0,20);
  L_Omega_1 ~ lkj_corr_cholesky(1);
  beta_1 ~ multi_normal(rep_vector(0, N), prior_dispersion_on_beta_1);
  for(k in 1:K){
    beta_0_k[k] ~ multi_normal(beta_0, Sigma_0);
    beta_1_k[k] ~ multi_normal(beta_1, Sigma_1);
    y_1[k] ~ multi_normal(treatment_quantiles_k[k], Sigma_y_k_1[k]);
    y_0[k] ~ multi_normal(beta_0_k[k], Sigma_y_k_0[k]);
}
}

generated quantities{

  vector[N] posterior_predictive_beta_0_k;
  vector[N] posterior_predictive_treatment_quantiles;
  vector[N] posterior_predictive_beta_1_k;
  posterior_predictive_beta_0_k = multi_normal_rng(beta_0, Sigma_0);
  posterior_predictive_beta_1_k = multi_normal_rng(beta_1, Sigma_1);
  posterior_predictive_treatment_quantiles = posterior_predictive_beta_0_k + posterior_predictive_beta_1_k;
}