
functions {
  real normal_ss_log(int N_c, int N_t, real y_control_sq_sum, real y_treatment_sq_sum, real y_control_sum, real y_treatment_sum, real mu_k, real tau_k, real sigma_control_k, real sigma_treatment_k) {

    real lp;
    lp <- -.5*((y_control_sq_sum - 2*mu_k*y_control_sum + N_c*mu_k^2)/sigma_control_k^2) - .5*N_c*log(sigma_control_k^2);
    lp <- lp -.5*((y_treatment_sq_sum - 2*(mu_k + tau_k)*y_treatment_sum + N_t*(mu_k + tau_k)^2)/sigma_treatment_k^2) - .5*N_t*log(sigma_treatment_k^2);
    return lp;
  }
}


data {
int N ;// number of observations
int K ;// number of sites
real y[N];// outcome variables of interest
int ITT[N];// intention to treat indicator
int site[N]; // site counter or indicator 
}


transformed data {
  int N_c_k[K];  // number of observations in control from site K
  int N_t_k[K];  // number of observations in treatment from site K
  real y_control_sq_sum[K];  // 
  real y_treatment_sq_sum[K];  //
  real y_control_sum[K];  //
  real y_treatment_sum[K];  //
  int s;

  // initialize everything to zero
  N_c_k <- rep_array(0, K);
  N_t_k <- rep_array(0, K);
  y_control_sq_sum <- rep_array(0.0, K);
  y_treatment_sq_sum <- rep_array(0.0, K);
  y_control_sum <- rep_array(0.0, K);
  y_treatment_sum <- rep_array(0.0, K);
  
  for (n in 1:N) {
    s <- site[n];
    N_c_k[s] <- N_c_k[s] + (1-ITT[n]);
    N_t_k[s] <- N_t_k[s] + (ITT[n]);
    y_control_sq_sum[s] <- y_control_sq_sum[s] + (1-ITT[n])*(y[n]^2);
    y_treatment_sq_sum[s] <- y_treatment_sq_sum[s] + ITT[n]*(y[n]^2);
    y_control_sum[s] <- y_control_sum[s] + (1-ITT[n])*(y[n]);
    y_treatment_sum[s] <- y_treatment_sum[s] + ITT[n]*(y[n]);
  
  }
}



parameters {


real mu_k[K];// mean
real tau_k[K] ;// mean TE
real musigma_k[K] ;// standard deviation of control group on log scale
real tausigma_k[K] ;// sd TE on log scale


}
transformed parameters {


real<lower=0> sigma_treatment_k[K] ;// standard deviation total for the treated in each site
real<lower=0> sigma_control_k[K] ;// standard deviation total for the control in each site


for (k in 1:K) {
sigma_control_k[k] = exp(musigma_k[k]);
sigma_treatment_k[k] = exp(musigma_k[k]+tausigma_k[k]);
}

}
model {



  for (k in 1:K) {
    
    // real normal_ss_log(int N_c, int N_t, real y_control_sq_sum, real y_treatment_sq_sum, real y_control_sum, real y_treatment_sum, real mu_k, real tau_k, real sigma_control_k, real sigma_treatment_k)

    increment_log_prob(normal_ss_log(N_c_k[k], N_t_k[k], y_control_sq_sum[k], y_treatment_sq_sum[k], y_control_sum[k], y_treatment_sum[k], mu_k[k], tau_k[k], sigma_control_k[k], sigma_treatment_k[k]));
  }

}



