

data {
  int<lower=0> K;  // number of sites
  int<lower=0> P;  // dimensionality of parameter vector which is jointly distributed - here, it is 4 dimensional
  real mu_k_hat[K] ; // means
  real tau_k_hat[K] ; // slopes
  real se_mu_k[K] ; // ses for mus
  real se_tau_k[K] ; // ses for taus
  real eta_k_hat[K] ; // means
  real gamma_k_hat[K] ; // slopes
  real se_eta_k[K] ; // ses for mus
  real se_gamma_k[K] ; // ses for taus
  matrix[P,P] theta_prior_sigma;
  vector[P] theta_prior_mean;
}


parameters {
  vector[P] theta;
  matrix[K,P] theta_k;
  corr_matrix[P] Omega;        //  correlation
  vector<lower=0>[P] nu;    //  scale
}
transformed parameters {
  matrix[P,P] sigma_theta;
  sigma_theta <- quad_form_diag(Omega,nu);
}

model {
 

  // parameter variance priors
  // XXX: change this?
  nu ~ cauchy(0,5); // 
  // theta ~ normal(0,100);
  Omega ~ lkj_corr(3); // pushes towards independence 

  // hyperparameter priors
  theta ~ multi_normal(theta_prior_mean, theta_prior_sigma);

  for (k in 1:K) {
    theta_k[k] ~ multi_normal(theta, sigma_theta);
    mu_k_hat[k] ~ normal(theta_k[k,1],se_mu_k[k]);
    tau_k_hat[k] ~ normal(theta_k[k,2],se_tau_k[k]);
    eta_k_hat[k] ~ normal(theta_k[k,3],se_eta_k[k]);
    gamma_k_hat[k] ~ normal(theta_k[k,4],se_gamma_k[k]);
  }
}

generated quantities{

vector[P] posterior_predicted_theta_k;

posterior_predicted_theta_k <- multi_normal_rng(theta, sigma_theta);

}
 
