clear
# delimit; 
cap clear matrix; 
set mem 500m; 
set maxvar 10000; 
cap log close; 
set more off;

*** SELECT MAIN DIRECTORY;
global MAIN_DIR="\\bbking2\rmeager\winprofile\mydocs\Research work\bayesian meta analysis\microcredit rct data\crepon et al 2015\Data&Code_AEJApp_MicrocreditMorocco";

global SUB_DIR="DoFiles";

cd "$MAIN_DIR";

*************************************;
* Outcomes;
*************************************;
* We create baseline outcomes;
do "$SUB_DIR\OutcomeConstruction_baseline_Oct2014.do";

* We create endline outcomes;
do "$SUB_DIR\OutcomeConstruction_endline_Oct2014.do";


************************************;
* Analysis;
************************************;
do "$SUB_DIR\Analysis_Oct2014.do";
do "$SUB_DIR\Graphs_Oct2014.do";


