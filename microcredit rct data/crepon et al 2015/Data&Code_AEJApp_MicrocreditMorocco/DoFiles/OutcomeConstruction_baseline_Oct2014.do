********************************************************************************************************************************************
**** DO FILE CONSTRUCS BASELINE IMPACT OUTCOMES
**** INPUT fILES:
**** Date: September 2014
********************************************************************************************************************************************
# delimit; 
clear;
cap clear matrix;
set mem 500m; 
set maxvar 10000; 
set more off;


use Input\Microcredit_BL_mini_anonym.dta, clear;

********************************************************************************************************
********************************************************************************************************
** 1.IMPACT OUTCOMES
********************************************************************************************************
********************************************************************************************************

********************************************************************************************************
********************************************************************************************************
** 1.1 Impact on targeted Outcomes 
********************************************************************************************************
********************************************************************************************************; 

********************************************************************************************************
** access to credit 
********************************************************************************************************; 
** I call formal the following: 1. Cr�dit agricole, 2. Autre banque, 4. Zakoura, 5. Fondation Cr�dit Agricole
6. Autre AMC (Association de Micro-cr�dit), 15. Coop�rative
** Informal is: 7. Usurier, 8. Bijoutier, 9. Famille, 10. Voisin 11. Amis, 12. Magasin, 13. Un client, 14. Un fournisseur, 
** 17. Cr�dit branchement; 


** Number of active loans : Al Amana , other formal , informal , total; 

foreach j of numlist 1(1)3 { ; 
gen alamana`j' = (i3_`j' == 3); 
gen other_formal`j' = (i3_`j' == 1 |i3_`j' == 2 |i3_`j' == 15 ); 
gen informal`j' = (i3_`j' == 7 |i3_`j' == 8 |i3_`j' == 9 |i3_`j' == 10 |i3_`j' == 11 |i3_`j' == 12 |i3_`j' == 13 |i3_`j' == 14);
gen branching`j' = (i3_`j' == 16 | i3_`j' == 17); 
gen other_amc`j' = (i3_`j' == 4 | i3_`j' == 5 | i3_`j' == 6); 
}; 

egen aloans_alamana = rowtotal(alamana1 alamana2 alamana3) ;
egen aloans_oformal = rowtotal(other_formal1 other_formal2 other_formal3) ; 
egen aloans_informal = rowtotal(informal1 informal2 informal3);
egen aloans_branching = rowtotal(branching1 branching2 branching3);  
egen aloans_oamc = rowtotal(other_amc1 other_amc2 other_amc3);
egen aloans_total = rowtotal(aloans_alamana aloans_oformal aloans_informal aloans_branching); 
egen aloans_oformal2 = rowtotal(aloans_oformal aloans_oamc); 

drop alamana* other_formal* informal* branching* other_amc*; 


** Amount of active loans : Al Amana , other formal , informal , total; 
foreach j of numlist 1(1)3 { ; 
gen alamana`j' = 0 ; 
gen other_formal`j' = 0 ; 
gen informal`j' = 0; 
gen branching`j' = 0; 
gen other_amc`j' = 0; 
replace alamana`j' = i9_`j' if (i3_`j' == 3) &  i9_`j' >= 0 ;
replace alamana`j' = . if (i3_`j' == 3) & i9_`j' < 0 ;
replace other_formal`j' = i9_`j' if (i3_`j' == 1 |i3_`j' == 2 |i3_`j' == 15 ) & (i9_`j' >= 0) ; 
replace other_formal`j' = . if (i3_`j' == 1 |i3_`j' == 2 |i3_`j' == 15 ) & (i9_`j' < 0) ; 
replace informal`j' = i9_`j' if (i3_`j' == 7 |i3_`j' == 8 |i3_`j' == 9 |i3_`j' == 10 |i3_`j' == 11 |i3_`j' == 12 |i3_`j' == 13 |i3_`j' == 14 ) 
& (i9_`j' >= 0);
replace informal`j' = . if (i3_`j' == 7 |i3_`j' == 8 |i3_`j' == 9 |i3_`j' == 10 |i3_`j' == 11 |i3_`j' == 12 |i3_`j' == 13 |i3_`j' == 14 ) 
& (i9_`j' < 0);
replace branching`j' = i9_`j' if (i3_`j' == 16 | i3_`j' == 17) & i9_`j' >= 0 ; 
replace branching`j' = . if (i3_`j' == 16 | i3_`j' == 17) & i9_`j' < 0 ; 
replace other_amc`j' = i9_`j' if (i3_`j' == 4 | i3_`j' == 5 | i3_`j' == 6) & i9_`j' >= 0 ; 
replace other_amc`j' = . if (i3_`j' == 4 | i3_`j' == 5 | i3_`j' == 6) & i9_`j' < 0 ; 
}; 

egen aloansamt_alamana = rowtotal(alamana1 alamana2 alamana3) ;
egen aloansamt_oformal = rowtotal(other_formal1 other_formal2 other_formal3) ; 
egen aloansamt_informal = rowtotal(informal1 informal2 informal3);
egen aloansamt_branching = rowtotal(branching1 branching2 branching3);  
egen aloansamt_oamc = rowtotal(other_amc1 other_amc2 other_amc3);
egen aloansamt_total = rowtotal(aloansamt_alamana aloansamt_oformal aloansamt_informal aloansamt_branching); 
egen aloansamt_oformal2= rowtotal(aloansamt_oformal aloansamt_oamc);

drop alamana* other_formal* informal* branching* other_amc*; 

foreach var in aloansamt_alamana aloansamt_informal aloansamt_branching {;
	_pctile `var', p(99.9);
	replace `var'=. if `var'>r(r1);
	}; 

_pctile aloansamt_oformal2, p(99.7);
replace aloansamt_oformal2=. if aloansamt_oformal2>r(r1);


** Number of past loans : Al Amana ,  other formal , informal ,  total;

foreach j of numlist 4(1)5 { ; 
gen alamana`j' = (i3_`j' == 3); 
gen other_formal`j' = (i3_`j' == 1 |i3_`j' == 2 |i3_`j' == 15); 
gen informal`j' = (i3_`j' == 7 |i3_`j' == 8 |i3_`j' == 9 |i3_`j' == 10 |i3_`j' == 11 |i3_`j' == 12 |i3_`j' == 13 |i3_`j' == 14 );
gen branching`j' = (i3_`j' == 16 |i3_`j' == 17); 
gen other_amc`j' = (i3_`j' == 4 | i3_`j' == 5 | i3_`j' == 6); 
}; 

egen ploans_alamana = rowtotal(alamana4 alamana5);
egen ploans_oformal = rowtotal(other_formal4 other_formal5) ; 
egen ploans_informal = rowtotal(informal4 informal5);
egen ploans_branching = rowtotal(branching4 branching5);  
egen ploans_oamc = rowtotal(other_amc4 other_amc5);  
egen ploans_total = rowtotal(ploans_alamana ploans_oformal ploans_informal ploans_branching); 
egen ploans_oformal2 = rowtotal(ploans_oformal ploans_oamc); 
 
drop alamana* other_formal* informal* branching* other_amc*; 


** Amount of past  loans : Al Amana , other formal , informal , total; 

foreach j of numlist 4(1)5 { ; 
gen alamana`j' = 0 ; 
gen other_formal`j' = 0 ; 
gen informal`j' = 0; 
gen branching`j' = 0; 
gen other_amc`j' = 0; 

replace alamana`j' = i9_`j' if (i3_`j' == 3) &  i9_`j' >= 0 ;
replace alamana`j' = . if (i3_`j' == 3) & i9_`j' < 0 ;
replace other_formal`j' = i9_`j' if (i3_`j' == 1 |i3_`j' == 2 |i3_`j' == 15 ) & (i9_`j' >= 0) ; 
replace other_formal`j' = . if (i3_`j' == 1 |i3_`j' == 2 |i3_`j' == 15 ) & (i9_`j' < 0) ; 
replace informal`j' = i9_`j' if (i3_`j' == 7 |i3_`j' == 8 |i3_`j' == 9 |i3_`j' == 10 |i3_`j' == 11 |i3_`j' == 12 |i3_`j' == 13 |i3_`j' == 14 ) 
& (i9_`j' >= 0);
replace informal`j' = . if (i3_`j' == 7 |i3_`j' == 8 |i3_`j' == 9 |i3_`j' == 10 |i3_`j' == 11 |i3_`j' == 12 |i3_`j' == 13 |i3_`j' == 14 ) 
& (i9_`j' < 0);
replace branching`j' = i9_`j' if (i3_`j' == 16 | i3_`j' == 17) & i9_`j' >= 0 ; 
replace branching`j' = . if (i3_`j' == 16 | i3_`j' == 17) & i9_`j' < 0 ; 
replace other_amc`j' = i9_`j' if (i3_`j' == 4 | i3_`j' == 5 | i3_`j' == 6) & i9_`j' >= 0 ; 
replace other_amc`j' = . if (i3_`j' == 4 | i3_`j' == 5 | i3_`j' == 6) & i9_`j' < 0 ; 
}; 

egen ploansamt_alamana = rowtotal(alamana4 alamana5);
egen ploansamt_oformal = rowtotal(other_formal4 other_formal5) ; 
egen ploansamt_informal = rowtotal(informal4 informal5); 
egen ploansamt_branching = rowtotal(branching4 branching5); 
egen ploansamt_oamc = rowtotal(other_amc4 other_amc5);  
egen ploansamt_total = rowtotal(ploansamt_alamana ploansamt_oformal ploansamt_informal ploansamt_branching); 
egen ploansamt_oformal2 = rowtotal(ploansamt_oformal ploansamt_oamc); 

drop alamana* other_formal* informal* branching* other_amc*; 


egen loans_alamana=rowtotal(aloans_alamana ploans_alamana);
egen loans_oformal=rowtotal(aloans_oformal ploans_oformal);
egen loans_informal=rowtotal(aloans_informal ploans_informal);
egen loans_branching=rowtotal(aloans_branching ploans_branching);
egen loans_oamc=rowtotal(aloans_oamc ploans_oamc);
egen loans_total=rowtotal(aloans_total ploans_total);
egen loans_oformal2=rowtotal(aloans_oformal ploans_oformal aloans_oamc ploans_oamc);

egen loansamt_alamana = rowtotal(aloansamt_alamana  ploansamt_alamana );
egen loansamt_oformal= rowtotal(aloansamt_oformal ploansamt_oformal);
egen loansamt_informal= rowtotal(aloansamt_informal ploansamt_informal);
egen loansamt_branching = rowtotal(aloansamt_branching ploansamt_branching ); 
egen loansamt_oamc = rowtotal(aloansamt_oamc ploansamt_oamc );
egen loansamt_total = rowtotal(aloansamt_total ploansamt_total ); 
egen loansamt_oformal2 = rowtotal(aloansamt_oformal2 ploansamt_oformal2);

gen client=.;

*** DUMMY of loans over the period ***;
foreach var in alamana oamc oformal informal branching total oformal2{;
	gen borrowed_`var'=0 if loans_`var'!=.;
		replace borrowed_`var'=1 if aloans_`var'>=1 & aloans_`var'!=.;
	}; 

********************************************************************************************************
** Credit constraints indicators
********************************************************************************************************; 

** why not try to borrow in the last 12 month
1. Pas besoin de cr�dit 
2. Peur que ma demande ne soit rejet�e
3. Montant des frais trop �lev� 
4. Taux d�int�r�t trop �lev�s 
5. Les antennes des organismes financiers sont trop loins de mon lieu de travail 
6. Montants propos�s par les organismes financiers trop faibles
7. Peur de ne pas pouvoir rembourser le cr�dit
8. Peur de perdre ma garantie 
9. Raisons religieuses
10. Pas de garantie � donner; 

gen nb_pasbesoin = i2701 == 1; 
gen nb_peurrejet = i2702 == 1; 
gen nb_frais = i2703 == 1; 
gen nb_taux = i2704 == 1; 
gen nb_distance = i2705 == 1; 
gen nb_montantinsuff = i2706 == 1; 
gen nb_peurremboursement = i2707 == 1; 
gen nb_peurpertegarantie = i2708 == 1; 
gen nb_religion = i2709 == 1; 
gen nb_pasgarantie= i2710 == 1; 
 
** credit demand refused; 
gen credit_refused = (i28==1); 
replace credit_refused = . if i28 == . | i28  <0 ; 

** need of credit not asked for; 
gen credit_notasked = (i32 == 1); 
replace credit_notasked = . if i32 == . | i32 < 0  ;

** need of credit today;
gen credit_need=i34==1;
replace credit_need=. if i34==.;

*** tried to take a credit in the last 12 months ;
gen try_credit=(i26==1);

********************************************************************************************************
** Credit Need
********************************************************************************************************; 

foreach var in fin form comm {;
foreach j of numlist 1(1)6 {;  
gen need_`var'_`j'  = .; 
};  
}; 

foreach j of numlist 1(1)6 {; 
replace need_fin_`j' = 1 if d5_`j' == 1 ; 
replace need_form_`j' = 1 if d5_`j' == 2; 
replace need_comm_`j' = 1 if d5_`j' == 3; 
replace need_fin_`j' = 0 if d5_`j' != 1 & d5_`j' != . & d5_`j' >= 0   ;  
replace need_form_`j' = 0 if d5_`j' != 2 & d5_`j' != . & d5_`j' >= 0   ;  
replace need_comm_`j' = 0 if d5_`j' != 3 & d5_`j' != . & d5_`j' >= 0   ;  
}; 


egen need_fin = rowmean(need_fin_1 need_fin_2 need_fin_3 need_fin_4 need_fin_5 need_fin_6); 
egen need_form = rowmean(need_form_1 need_form_2 need_form_3 need_form_4 need_form_5 need_form_6); 
egen need_comm = rowmean(need_comm_1 need_comm_2 need_comm_3 need_comm_4 need_comm_5 need_comm_6); 

drop need_fin_* need_form_* need_comm_*; 

********************************************************************************************************

********************************************************************************************************
** INVESTMENTS (in dhs)
********************************************************************************************************; 
** Agriculture Investment;

gen inv_agri = 0; 
foreach j of numlist 1(1)16 { ; 
replace inv_agri = inv_agri + e7_`j' if e7_`j'!= . & e7_`j' >= 0 & e3_`j' == 1; 
replace inv_agri = inv_agri + e7_`j'*e5_`j'/100 if e7_`j'!= . & e7_`j' >= 0 & e5_`j'!= . & e5_`j' >= 0 & e3_`j' == 2; 
} ; 


** Valorisation of agricultural assets?; 

foreach j of numlist 1(1)16 { ;
replace e2_`j'=0 if e3_`j'!=1 & e3_`j'!=2; 
sum e7_`j', d;
gen ag_`j'=r(p50)*e2_`j';
sum e7_`j', d;
replace ag_`j'= r(p50)*e2_`j'*e5_`j'/100 if e5_`j'!= . & e5_`j' >= 0; 
};

egen asset_agri=rsum(ag_1-ag_16);
	drop ag_1-ag_16;
 
** Land ; 

gen land_ha = c6;
replace land_ha = 0 if c6 == . ;
 
gen land2_ha=e9;
replace land2_ha=0 if e9==.;
recode land2_ha (-98=.);

gen land_other_ha=e11;
replace land_other_ha=0 if e11==.;

gen land=(land_ha>0 &  land_ha!=0);

egen land_expl_ha = rsum(land2_ha land_other_ha);   
	drop land2_ha land_other_ha;

** Livestock animal assets, last year; 

gen inv_livestockanim = 0; 
foreach j of numlist 1(1)8 { ; 
replace inv_livestockanim  = inv_livestockanim  + f13_`j' if f13_`j' != . & f13_`j' >= 0 ; 
}; 

 
** Livestock material assets (last year) ; 

gen inv_livestockmat = 0 ; 
foreach j of numlist 1(1)5 { ; 
replace inv_livestockmat = inv_livestockmat + f4_`j' if f4_`j' != . & f4_`j' >= 0; 
}; 
 
egen inv_livestock = rowtotal(inv_livestockmat inv_livestockanim); * per last year; 


* Value of stock of livestock assets; 
	foreach j of numlist 1(1)3 { ;
		gen assetlive`j'=0;
		gen unitprice`j' = f4_`j' / f2_`j' if f4_`j'>0 & f4_`j'!=. & f2_`j'>0 & f2_`j'!=.;
		sum unitprice`j' if unitprice`j'>0, detail;
		replace assetlive`j'=r(p50)*f2_`j' if f2_`j'>0 & f2_`j'!=.;
		};
	egen asset_livestock = rsum(assetlive1-assetlive3);
		drop assetlive1-unitprice3;	 
 

** Business assets ; 
	* rule: if someone owns it alone, we consider full purchase amount as the investment. If someone owns it jointly with others, we assign him a fraction of the
	expense corresponding to his fraction of the ownership; 
	** final measure is by year;


gen inv_business_assets = 0; 

foreach j of numlist 1(1)13 { ; 
replace inv_business_assets = inv_business_assets + g7_`j' if g7_`j' >= 0 & g7_`j' != . & g4_`j' == 1 & g6_`j' == 1; 
replace inv_business_assets = inv_business_assets + g7_`j'*g5_`j'/100 if g7_`j' >= 0 & g7_`j' != . & g5_`j' >= 0 & g5_`j' != .  & g4_`j' == 2 & g6_`j' == 1; 
}; 

** Business inputs  - this is by month; 
gen inv_business_inputs = 0 ; 
foreach j of numlist 1(1)6 {; 
foreach i of numlist 1(1)5 {;  
replace inv_business_inputs = inv_business_inputs + g18_`j'_`i'*g19_`j'_`i'*12 if g19_`j'_`i' != . & g19_`j'_`i' >= 0 & g18_`j'_`i' != . & g18_`j'_`i' >= 0 ;  
}; 
}; 

foreach j of numlist 1(1)6 {; 
foreach i of numlist 1(1)5 {;  
replace inv_business_inputs = inv_business_inputs + g24_`j'_`i'*12 if g24_`j'_`i' != . & g24_`j'_`i' >= 0  ;
}; 
}; 

* Value of business assets;
	foreach j of numlist 1(1)9 { ;
		gen assetbus`j'=0;
		sum g7_`j' if g7_`j' > 0, detail;
		replace assetbus`j'=r(p50)*g2_`j' if g2_`j'>0 & g2_`j'!=. & g4_`j' == 1 ;
		replace assetbus`j'=r(p50)*g2_`j'*g5_`j'/100 if g2_`j'>0 & g2_`j'!=. & g4_`j' == 2 ;
		};
		gen assetbus10 = g7_10 if g7_10>0 & g7_10!=. & g4_10 == 1;
			replace assetbus10 = g7_10*g5_10/100 if g2_10>0 & g2_10!=. & g5_10 >= 0 & g5_10 != . & g4_10 == 2 ;
		gen assetbus11 = g7_11 if g7_11>0 & g7_11!=. & g4_11 == 1;
			replace assetbus11 = g7_11*g5_11/100 if g2_11>0 & g2_11!=. & g5_11 >= 0 & g5_11 != . & g4_11 == 2 ;
		gen assetbus12 = g7_12 if g7_12>0 & g7_12!=. & g4_12 == 1;
			replace assetbus12 = g7_12*g5_12/100 if g2_12>0 & g2_12!=. & g5_12 >= 0 & g5_12 != . & g4_12 == 2 ;
		gen assetbus13 = g7_13 if g7_13>0 & g7_13!=. & g4_13 == 1;
			replace assetbus13 = g7_13*g5_13/100 if g2_13>0 & g2_13!=. & g5_13 >= 0 & g5_13 != . & g4_13 == 2 ;

egen asset_business = rsum(assetbus1-assetbus13);
	drop assetbus1-assetbus13;	 


** We trim inv variables; 
foreach var in inv_livestock inv_business_assets inv_business_inputs asset_agri asset_livestock asset_business {;
	_pctile `var', p(99.7);
	replace `var'=. if `var'>r(r1);
	};

	_pctile inv_agri , p(99.6);
	replace inv_agri =. if inv_agri >r(r1);


********************************************************************************************************
********************************************************************************************************
** 1.2 Impact on Final Outcomes 
********************************************************************************************************
********************************************************************************************************; 
********************************************************************************************************
** Activities
********************************************************************************************************; 

gen act_agri = 0; 
gen act_livestock =  0; 
gen act_business = 0 ; 

foreach j of numlist 1(1)6 {; 
gen d1_100code`j' = d1_code`j'/100; 
replace act_agri = 1 if d1_nom`j' != "" & d1_nom`j' != "." & d2_`j' == 1; 
replace act_agri = 1 if d2_`j' == 1; 
replace act_agri = 1 if d2_`j' == . & d1_nom`j' != "" & d1_nom`j' != "." & (d1_100code`j' < 2 & d1_100code`j' > 0); 
replace act_livestock = 1 if d1_nom`j' != "" &  d1_nom`j' != "." & d2_`j' == 2; 
replace act_livestock = 1 if d2_`j' == 2; 
replace act_livestock = 1 if d2_`j' == . & d1_nom`j' != "" & d1_nom`j' != "." & (d1_100code`j' < 3 & d1_100code`j' >= 2); 
replace act_business = 1 if d1_nom`j' != "" & d1_nom`j' != "." & (d2_`j' == 3 |d2_`j' == 4 |d2_`j' == 5 | d2_`j' == 6);
replace act_business = 1 if d2_`j' == . & d1_nom`j' != "" & d1_nom`j' != "." & (d1_100code`j' < 7 & d1_100code`j' >= 3); 
drop d1_100code`j' ;
}; 

** Number of self-employment activities;
gen act_number = 0 ; 
foreach j of numlist 1(1)6 {; 
replace act_number = act_number + 1 if d1_nom`j' != "" & d1_nom`j' != "." & d2_`j' != 7 & d2_`j' != 8 ;
}; 


* Diversification, as declared in specific production modules; 
gen act_number_agri1 = 0 ; 
foreach j of numlist 1(1)12 {;
replace act_number_agri1 = act_number_agri1 + 1 if e15_`j' ==1 ;
}; 

gen act_number_agri2 = 0 ;
foreach i of numlist 1(1)11 {; 
replace act_number_agri2 = act_number_agri2 + 1 if e25_`i'>0 & e25_`i'!=. ;
};

gen act_number_agri3 = 0 ; 
foreach j of numlist 1(1)6 {;
replace act_number_agri3 = act_number_agri3 + 1 if e36_`j' ==1 ;
};

egen act_total_agri=rsum(act_number_agri1  act_number_agri2 act_number_agri3); 
	drop act_number_agri1  act_number_agri2 act_number_agri3; 


gen act_number_live1 = 0 ; 
foreach j of numlist 1(1)8 {;
replace act_number_live1 = act_number_live1 + 1 if f8_0_`j' ==1 ;
}; 

gen act_number_live2 = 0 ; 
foreach j of numlist 1(1)3 5 6 4 {;
replace act_number_live2 = act_number_live2 + 1 if f15_0_`j' ==1 ;
}; 

egen act_total_live=rsum(act_number_live1  act_number_live2 ); 
	drop act_number_live1  act_number_live2;


gen act_number_business = 0 ; 
	foreach j of numlist 1(1)6 {; 
	replace act_number_business = act_number_business + 1 if d1_nom`j' != "" & d1_nom`j' != "." & (d2_`j' == 3 |d2_`j' == 4 |d2_`j' == 5 | d2_`j' == 6) ;
	}; 



** Dummies for self-employment activities;

gen selfempl_agri = (act_total_agri!=0);
	replace selfempl_agri = . if act_total_agri==.;

gen selfempl_livestock = (act_total_live!=0);
	replace selfempl_livestock=. if act_total_live==.;

gen selfempl_business = (act_number_business!=0);
	replace selfempl_business = . if act_number_business==.;

gen self_empl=(selfempl_agri==1 | selfempl_livestock==1 | selfempl_business==1);


*****************************************************************
** Sales of agricultural production & value of production;; 
*****************************************************************; 
* We count as sales: total value sold before and after harvest, valued at their own price; 
* Plus own consumption, valued at median village price; 

	sort id3; 
	foreach j of numlist 1(1)5 7 9 11 13 15 17 19  {; 
	egen price`j' = median(e23_`j') if e23_`j' >= 0 ;
	egen price2`j' = median(e21_`j') if e21_`j' >= 0 ;
	replace price`j' = price2`j' if price`j' == . ; 
	}; 

gen sale_cereal = 0; 
foreach j of numlist 1(1)5 7 9 11 13 15 17 19 {; 
replace sale_cereal = sale_cereal + e20_`j'*e21_`j' if e20_`j' >= 0 & e20_`j' != . & e21_`j' != . & e21_`j' >=0  ;
replace sale_cereal = sale_cereal + e22_`j'*e23_`j' if e22_`j' >= 0  &  e22_`j' != . & e23_`j' != . & e23_`j' >=0 ;
replace sale_cereal = sale_cereal + e19_`j'*price`j' if e19_`j' >= 0  &  e19_`j' != . & price`j' != . & price`j' >=0 ;
drop price`j' price2`j';
}; 

	foreach j of numlist 1(1)13 {; 
	egen price`j' = median(e30_`j') if e30_`j' >= 0 ;
	egen price2`j' = median(e32_`j') if e32_`j' >= 0 ;
	replace price`j' = price2`j' if price`j' == . ; 
	}; 

gen sale_tree = 0 ; 
foreach j of numlist 1(1)13 {; 
replace sale_tree = sale_tree + e29_`j'*e30_`j'  if e29_`j' >= 0  & e29_`j' != . & e30_`j' != . & e30_`j' >=0  ;
replace sale_tree = sale_tree + e31_`j'*e32_`j' if e31_`j' >= 0  & e31_`j' != . & e32_`j' != . & e32_`j' >=0  ;
replace sale_tree = sale_tree + e33_`j'*e34_`j' if e33_`j' >= 0  & e33_`j' != . & e34_`j' != . & e34_`j' >=0 ;  
replace sale_tree = sale_tree + e28_`j'*price`j' if e28_`j' >= 0  & e28_`j' != .  & price`j' != . & price`j' >=0 ;  
drop price`j' price2`j';
}; 

	foreach j of numlist 1(1)8 {; 
	egen price`j' = median(e40_`j') if e40_`j' >= 0 ;
	}; 

gen sale_veg = 0 ; 
foreach j of numlist 1(1)8 {; 
replace sale_veg = sale_veg + e39_`j'*e40_`j' if e39_`j' != . & e39_`j' >= 0 & e40_`j' != . & e40_`j' >=0 ; 
replace sale_veg = sale_veg + e38_`j'*price`j' if e38_`j' != . & e38_`j' >= 0  & price`j' != . & price`j' >=0 ;  
drop price`j';
}; 


gen sale_agri = sale_cereal + sale_tree + sale_veg ; 


_pctile sale_agri, p(99.6); 
replace sale_agri = . if sale_agri> r(r1); 


********************************************************************************************************
** Livestock Sales
********************************************************************************************************; 
* we count as sales the quantity sold and the quantity auto-consumed; 

** Sales livestock animals;

foreach j of numlist 1(1)8 {; 
gen price`j'_temp = f9_`j'/f8_`j' if f8_`j' != . & f8_`j' >= 0 & f9_`j' != . & f9_`j' >= 0; 
}; 

foreach j of numlist 1(1)8 {; 
egen price`j' = median(price`j'_temp); 
}; 

gen sale_livestockanim = 0; 
foreach j of numlist 1(1)8 {; 
replace sale_livestockanim = sale_livestockanim + f9_`j' if f9_`j' != .  & f9_`j' >= 0 ; 
replace sale_livestockanim = sale_livestockanim + f10_`j'*price`j' if f10_`j' >= 0  & f10_`j' != . & price`j' != . & price`j' >= 0  ; 
drop price`j' price`j'_temp; 
}; 


*** Sales livestock products;

* Sale of milk, butter, honey, etc...; 
* again, we count as sale the amount sold plus the amount auto consumed; 

foreach j of numlist 1(1)6 {; 
	gen unitprice`j' = f18_`j'/f17_`j' if f18_`j' >= 0 & f17_`j' >= 0 & f18_`j' != . & f17_`j' != . ; 
	egen price`j' = median(unitprice`j') if unitprice`j' >= 0 ;
	drop unitprice`j'; 
	}; 

gen sale_livestockprod = 0 ; 
foreach j of numlist 1(1)6 {;
replace sale_livestockprod = sale_livestockprod + f18_`j' if f18_`j' >= 0  & f18_`j' != . ; 
replace sale_livestockprod = sale_livestockprod + f16_`j'*price`j' if f16_`j' >= 0  & f16_`j' != . ; 
drop price`j';
}; 

* Total sales from livestock activities; 

gen sale_livestock = sale_livestockanim + sale_livestockprod; 


_pctile sale_livestock, p(99.7); 
replace sale_livestock = . if sale_livestock> r(r1); 

replace sale_livestockanim=. if sale_livestock==.;
replace sale_livestockprod=. if sale_livestock==.;


************************************************************************************************************
******** Business Sales; 
************************************************************************************************************; 

** Business sales (last week and last months), sale_business is yearly ; 

gen sale_business = 0; 
foreach j of numlist 1(1)3 {; 
foreach i of numlist 1(1)6 {; 
replace sale_business = sale_business + g28_`j'_`i' * g29_`j'_`i'*12 if g28_`j'_`i' != . & g29_`j'_`i' != . & g28_`j'_`i' >=0 & g29_`j'_`i' >=0 ; 
}; 
}; 

foreach j of numlist 1(1)4 {; 
replace sale_business = sale_business + g35_1_`j'*12 if g35_1_`j' != . & g35_1_`j' >=0; 
}; 

foreach j of numlist 1(1)4 {; 
replace sale_business = sale_business + g35_2_`j' if g35_2_`j' != . & g35_2_`j' >=0; 
}; 


_pctile sale_business, p(99.6); 
replace sale_business = . if sale_business> r(r1); 

********************************************************************************************************
** Expenses
********************************************************************************************************; 
*************************;
** Agricultural Expenses; 

* Inputs expenses only; 

gen expense_agriinputs = 0 ; 

foreach j of numlist 1(1)12 {; 
replace expense_agriinputs  = expense_agriinputs + e49_`j' if e49_`j' != . & e49_`j'>=0 ; 
}; 

* Labor wages and expenses; 
 
gen expense_agrilabor =0 ; 
replace expense_agrilabor = expense_agrilabor + e44*e45*12 if e45 != . & e45 >= 0 & e44 != . & e45 >= 0 ; 
replace expense_agrilabor = expense_agrilabor + e46*(e47_r*e48_r) if e46 != . & e46 >= 0 &  e47_r != . & e47_r >= 0 & e48_r != . & e48_r >= 0  ;
replace expense_agrilabor = expense_agrilabor + e46*(e47_hr*e48_hr) if e46 != . & e46 >= 0 &  e47_hr != . & e47_hr >= 0 & e48_hr != . & e48_hr >= 0  ;

* Investment Expenses; 
gen expense_agriinv = inv_agri ;

* Rental Expenses; 

gen expense_agrirent = 0; 
foreach j of numlist 1(1)16 {; 
replace expense_agrirent = expense_agrirent + e4_`j' if e4_`j' != . & e4_`j' >= 0 ; 
}; 

* Total agricultural expenses; 

gen expense_agri = expense_agriinputs + expense_agrilabor + expense_agriinv + expense_agrirent; 

_pctile expense_agri, p(99.7); 
replace expense_agri= . if expense_agri> r(r1); 

*********************;
** Livestock Expenses; 

* Material rental expenses; 
gen expense_livestockmatrent = 0; 
foreach j of numlist 1(1)5 { ; 
replace expense_livestockmatrent = expense_livestockmatrent + f7_`j' if f7_`j' != . & f7_`j' >= 0; 
}; 

* Material investment expenses; 

gen expense_livestockmatinv = 0; 
foreach j of numlist 1(1)5 { ; 
replace expense_livestockmatinv = expense_livestockmatinv + f4_`j' if f4_`j' != . & f4_`j' >= 0 & f3_`j' == 1; 
}; 

* Animal investment expenses; 
gen expense_livestockaniminv = inv_livestockanim; 

* Labor expenses for livestock activity; 

gen expense_livestocklabor = 0; 
replace expense_livestocklabor = expense_livestocklabor + f22 *f23 *12 if f22 != . & f22 >= 0 & f23 != . & f23 >= 0 ; 
replace expense_livestocklabor = expense_livestocklabor + f24 *f25 *f26 if f24 != . & f24 >= 0 & f25 != . & f25 >= 0 & f26 != . & f26 >= 0 ; 

* Inputs expenses; 
gen expense_livestockinputs = 0; 
foreach j of numlist 1(1)3 {; 
replace expense_livestockinputs = expense_livestockinputs + f27_`j' *12 if f27_`j' != . & f27_`j' >= 0 ; 
replace expense_livestockinputs = expense_livestockinputs + f28_`j'  if f28_`j' != . & f28_`j' >= 0 ; 
}; 

foreach j of numlist 4(1)6 {; 
replace expense_livestockinputs = expense_livestockinputs + f27_`j'm *12 if f27_`j'm != . & f27_`j'm >= 0 ; 
replace expense_livestockinputs = expense_livestockinputs + f28_`j'm  if f28_`j'm != . & f28_`j'm >= 0 ; 
}; 

* Total livestock expenses;

gen expense_livestock = expense_livestockmatrent + expense_livestockmatinv + expense_livestockaniminv + expense_livestocklabor + expense_livestockinputs; 

_pctile expense_livestock, p(99.7); 
replace expense_livestock= . if expense_livestock> r(r1); 


************************************
** Business Expenses; 

* Material Rental Expenses; 
gen expense_busrent = 0; 
foreach j of numlist 1(1)13 {; 
replace expense_busrent = expense_busrent + g9_`j' if g9_`j' != . & g9_`j' >= 0 ; 
}; 

gen expense_businv = inv_business_assets; 

gen expense_businputs = inv_business_inputs; 

gen expense_buslabor = 0; 
foreach j of numlist 1(1)3 {; 
replace expense_buslabor = expense_buslabor + g47_`j'*g48_`j'*12 if g47_`j'!= . & g48_`j'!= . & g47_`j'>= 0 & g48_`j'>= 0;  
replace expense_buslabor = expense_buslabor + g49_`j'*g50_`j'*g51_`j' if g49_`j'!= . & g50_`j'!= . & g51_`j'!= . & g49_`j'>= 0 & g50_`j'>= 0 & g51_`j'>= 0;  
}; 

gen expense_business = expense_busrent + expense_businv + expense_businputs + expense_buslabor; 


_pctile expense_business, p(99.7); 
replace expense_business= . if expense_business> r(r1); 


********************************************************************************************************
** Savings
********************************************************************************************************; 
* In kind Savings, using  the median prices; 

* Agricultural savings; 
foreach j of numlist 1(1)5 7 9 11 13 15 17 19 {; 
	replace e21_`j' = . if e21_`j' < 0 ; 
	by id3: egen price_`j' = median(e21_`j'); 
	}; 

gen savings_cereal = 0; 
	foreach j of numlist 1(1)5 7 9 11 13 15 17 19 {; 
	replace savings_cereal = savings_cereal + e24_`j'*price_`j'  if e24_`j' != . & e24_`j' >= 0 & price_`j' != . & price_`j' >= 0  ; 
	drop price_`j'; 
	}; 

* e30 e32 and e34; 
foreach j of numlist 1(1)13 {; 
	replace e30_`j' = . if e30_`j' < 0; 
	replace e32_`j' = . if e32_`j' < 0; 
	replace e34_`j' = . if e34_`j' < 0; 
	egen price_sale_`j' = median(e30_`j') ; 
	egen price_sale2_`j' = median(e32_`j');
	egen price_sale3_`j' = median(e34_`j'); 
	gen price_`j' = price_sale3_`j'; 
	replace price_`j' = price_sale2_`j' if price_sale3_`j' == . & price_sale2_`j' != . ;
	replace price_`j' = price_sale_`j' if price_sale3_`j' == . & price_sale2_`j' == . & price_sale_`j' != . ; 
	}; 

gen savings_tree = 0; 
	foreach j of numlist 1(1)13 {; 
	replace savings_tree = savings_tree + e35_`j'*price_`j'  if e35_`j' != . & e35_`j' >= 0 & price_`j' != . & price_`j' >= 0   ; 
	drop price_sale_`j' price_sale2_`j' price_sale3_`j' price_`j'; 
	}; 

foreach j of numlist 1(1)8 {;
	replace e40_`j' = . if e40_`j' < 0 ; 
	egen price_sale_`j' = median(e40_`j'); 
	gen price_`j' = price_sale_`j'; 
	}; 

gen savings_veg = 0; 
foreach j of numlist 1(1)8 {; 
	replace savings_veg = savings_veg + e40b`j'*price_`j'  if e40b`j' != . & e40b`j' >= 0 & price_`j' != . & price_`j' >= 0  ; 
	drop price_sale_`j' price_`j'; 
	}; 


gen savings_agri = savings_cereal + savings_tree + savings_veg ; 

_pctile savings_agri, p(99.7); 
replace savings_agri= . if savings_agri> r(r1); 

*********************;
** Livestock savings;
* calculating in-kind savings in terms of cattle; 
* f13 = total purchase amount (so need to divide by animals sold to get the price);
* f12 = animals bought ; 
* f9 = total sale amount; 
* f8 = animals sold; 
* f14 = number of animals left; 


foreach j of numlist 1(1)8 {;
replace f13_`j' = . if f13_`j' < 0 ; 
replace f12_`j' = . if f12_`j' < 0 ;  
replace f9_`j' = . if f9_`j' <  0 ; 
replace f8_`j' = . if f8_`j' < 0 ; 
egen price1_`j' = median(f13_`j'/f12_`j') ;
egen price2_`j' = median(f9_`j'/f8_`j') ;  
}; 

gen savings_livestock = 0; 
foreach j of numlist 1(1)8 {; 
replace savings_livestock = savings_livestock + f14_`j'*price1_`j'  if f14_`j' != . & f14_`j' >= 0 & price1_`j' != .  ;
replace savings_livestock = savings_livestock + f14_`j'*price2_`j'  if f14_`j' != . & f14_`j' >= 0 & price1_`j' == . & price2_`j' != .   ;
}; 


_pctile savings_livestock, p(99.7); 
replace savings_livestock= . if savings_livestock> r(r1);


gen lost_livestockanim = 0; 
foreach j of numlist 1(1)8 {; 
replace lost_livestockanim  = lost_livestockanim  + f11_`j'*price1_`j'  if f11_`j' != . & f11_`j' >= 0 & price1_`j' != . ;
replace lost_livestockanim  = lost_livestockanim  + f11_`j'*price2_`j'  if f11_`j' != . & f11_`j' >= 0 & price1_`j' == . & price2_`j' != . ;
drop price1_`j' price2_`j'; 
}; 

gen hadlost_livestockanim = 0 if lost_livestockanim!=.;
	replace hadlost_livestockanim = 1 if lost_livestockanim>0 & lost_livestockanim!=.;   


* Livestock production inventory; 

foreach j of numlist 1(1)6 {; 
replace f17_`j' = . if f17_`j' < 0 ; 
replace f18_`j' = . if f18_`j' < 0 ;  
egen price_`j' = median(f18_`j'/f17_`j'); 
}; 

gen savings_livestock_prod = 0; 
foreach j of numlist 1(1)6 {; 
replace savings_livestock_prod = savings_livestock_prod + f18b`j'*price_`j'  if f18b`j' != . & f18b`j' >= 0 ; 
drop price_`j'; 
}; 


* Savings in terms of business inventory; 

gen savings_business = 0 ; 
foreach j of numlist 1(1)6 {; 
replace savings_business = savings_business + g39_`j' if g39_`j' != . & g39_`j' >= 0 ; 
}; 


_pctile savings_business, p(99.7); 
replace savings_business= . if savings_business> r(r1);

***************************************************************;
********************************;
*** AGGREGATES FOR ALL ACTIVITY ;

********************************************************************************************************
** Profits
********************************************************************************************************; 
	gen prod_agri= sale_agri + savings_agri;

	gen profit_agri = prod_agri-expense_agri;

	gen profit_livestock = sale_livestock - expense_livestock; 

	gen profit_business = sale_business - expense_business ; 

	gen astock_livestock = savings_livestock + asset_livestock ;

	gen stock_agri=savings_agri + asset_agri;

gen inv_total= inv_business_assets + inv_livestock + inv_agri;

gen output_total= prod_agri + sale_business + sale_livestock;

gen expense_total= expense_agri + expense_business + expense_livestock;

gen profit_total = profit_agri + profit_livestock + profit_business;

gen assets_total = asset_agri + astock_livestock + asset_business; 


********************************************************************************************************
** Consommation
********************************************************************************************************; 

** Monthly food consumption; 

gen cons_food = 0 ; 
foreach j of numlist 1(1)14 17 18 {; 
replace cons_food = cons_food + h1_`j' if h1_`j' != . & h1_`j' >= 0  ; 
replace cons_food = cons_food + h2_`j'  if h2_`j' != . & h2_`j'  >= 0 ;
}; 

replace cons_food = cons_food *4.345 ; 

gen cons_food_usual = h8*4.345; 
replace cons_food_usual = . if h8 == . | h8 < 0; 


** Monthly durables consumption (I use SECTION C which has it per 12 months); 

gen cons_durables = 0; 
	foreach j of numlist 1(1)31 {; 
	replace cons_durables = cons_durables + c4_`j' if c4_`j' >= 0  & c4_`j' != . ; 
	};
	replace cons_durables=0 if cons_durables==.; 
	replace cons_durables = cons_durables/12 ; 

** Monthly non-durables consumption; 
gen cons_nondurables = 0 ; 

	replace cons_nondurables = cons_nondurables + cons_food; 

	foreach j of numlist 15 16 19(1)21 {; 
	replace cons_nondurables  = cons_nondurables + h1_`j'*4.345 if h1_`j' != . & h1_`j' >= 0 ; 
	}; 

	foreach j of numlist 15 19(1)21 {; 
	replace cons_nondurables = cons_nondurables + h2_`j'*4.345 if h2_`j' != . & h2_`j' >= 0 ; 
	}; 

	foreach j of numlist 1(1)9 11(1)17 { ; 
	replace cons_nondurables = cons_nondurables + h3_`j' if h3_`j' != . & h3_`j' >= 0 ; 
	} ; 

	foreach j of numlist 1(1)10 { ; 
	replace cons_nondurables = cons_nondurables + h4_`j'/12 if h4_`j' != . & h4_`j' >= 0  ; 
	} ; 

	foreach j of numlist 1(1)9 { ; 
	replace cons_nondurables = cons_nondurables + h6_`j'/12 if h6_`j' != . & h6_`j' >= 0  ; 
	replace cons_nondurables = cons_nondurables + 0 if h6_`j' == . ;
	} ; 

***credit repayment;
gen cons_repay= 0; 
	replace cons_repay= cons_repay + h3_10 if h3_10!=. & h3_10>=0;  

** Now, for each household with zero food consumption or total consumption, we will put all items to missing; 
replace cons_food = . if cons_food == 0 ;  


* TOTAL Consumption (monthly); 
	gen consumption = cons_durables + cons_nondurables; 

	replace consumption=. if cons_food == . ;   
	replace consumption =. if consumption < 200 ;

_pctile consumption, p(99.9); 
replace consumption = . if consumption > r(r1); 

	foreach var in food durables nondurables repay  {; 
	replace cons_`var' = . if consumption == . ; 
	}; 

_pctile cons_repay, p(99.9); 
replace cons_repay = . if cons_repay > r(r1); 


** Total number of household members;

gen members_resid = 0; 
	foreach j of numlist 1(1)17 {; 
	replace members_resid = members_resid + 1 if ((a5_`j' == 3 | a5_`j'==5))| ((a5_`j' != 3 & a5_`j'!=5) & a3_`j' == 1) ; 
	};
gen nadults_resid = 0; 
	foreach j of numlist 1(1)17 {; 
	replace nadults_resid = nadults_resid + 1 if a7_`j' >= 16 & a7_`j' !=. & (((a5_`j' == 3 | a5_`j'==5))| (a5_`j' != 3 & a5_`j'!=5 & a3_`j' == 1)) ; 
	};
gen nchildren_resid = 0;
	foreach j of numlist 1(1)17 {;
	replace nchildren_resid = nchildren_resid + 1 if a7_`j' < 16 & a7_`j' >= 0 & (((a5_`j' == 3 | a5_`j'==5))| (a5_`j' != 3 & a5_`j'!=5 & a3_`j' == 1)) ; 
	};

*****************************************;
** Per capita consumption/poverty *******;
*****************************************;

foreach var in food nondurables durables {;
	gen conspc_`var' = cons_`var' / members_resid;
		replace conspc_`var'= conspc_`var' * 177.798/174.25 if wave ==1 | wave ==2; 
		};

gen consumption_pc = consumption / members_resid;
	replace consumption_pc = consumption_pc * 177.798/174.25 if wave ==1 | wave ==2; 

gen poor = (consumption_pc < 297.42 );
	replace poor = . if consumption_pc == .;


********************************************************************************************************
** Income
********************************************************************************************************; 
# delimit; 

gen income_agri = profit_agri ; 
gen income_livestock = profit_livestock; 
gen income_business = profit_business; 

** Salary income from Section H2; 

gen income_dep = 0 ; 
foreach j of numlist 1(1)17 {;
replace income_dep = income_dep + h11_`j' if h11_`j' != . & h11_`j' >= 0 ;
replace income_dep = income_dep + h11c`j' if h11c`j' != . & h11c`j' >= 0 ;
};

gen daily_dum=0;
	replace daily_dum=1 if income_dep!=0 & income_dep!=.; 

** Pension and asset sales;
gen income_other = 0 ; 
foreach j of numlist 2(1)6 {; 
foreach i of numlist 1(1)17 {;
replace income_other = income_other + h15_`j' if h15_`j' != . & h15_`j' >= 0 ;
replace income_other = income_other + h13_`i' if h13_`i' != . & h13_`i' >= 0 ;
}; 
};

gen income_pension = 0 ; 
foreach i of numlist 1(1)17 {;
replace income_pension = income_pension + h13_`i' if h13_`i' != . & h13_`i' >= 0 ;
};

gen income_assetsales = 0 ;
foreach j of numlist 1(1)1 {;
replace income_assetsales = income_assetsales + h15_`j' if h15_`j' != . & h15_`j' >= 0 ;
};

* Remittances;
gen income_remittance = 0 ; 
foreach j of numlist 2(1)3{; 
replace income_remittance = income_remittance + h15_`j' if h15_`j' != . & h15_`j' >= 0 ;
}; 


* Yearly aggregate household income; 
gen income_gvt = . ; 

egen income = rsum(income_agri income_livestock  income_business income_dep income_other income_remittance);

_pctile income, p(99.7);
replace income=. if income> r(r1); 


* Shares for extended set of controls;
egen income_rest = rsum(income_remittance income_pension income_gvt income_assetsales);

egen income_self = rsum(income_agri income_livestock income_business);

gen share_inc_self = income_self / income;
gen share_inc_dep = income_dep / income;
gen share_inc_rest = income_rest / income;
gen share_cons_income = consumption / income; 

********************************************************************************************************
** Assets
********************************************************************************************************; 
** create dummies for assets; 
foreach j of numlist 1(1)28 {; 
gen asset_`j' = (c1_`j' == 1); 
}; 

*** create a simple index;
pca asset_1-asset_28, factor(1);
predict score;
matrix e= r(scoef)';
svmat e, name(coef); 
matrix list e;
forvalues i=1(1)28{;
scalar define xcoef`i'=coef`i';
};
forvalues j=1(1)28{;
sum asset_`j', detail;
scalar xa = r(mean); 
scalar xb = r(sd); 
gen score_`j'=xcoef`j'*(asset_`j'-xa)/xb;
scalar drop xa xb;
};
egen assetindex=rsum(score_1-score_28);  

	drop asset_1-asset_28  score coef1- coef28 score_1-score_28; 

********************************************************************************************************
** Employees
********************************************************************************************************; 

** Agricultural Employees; 
gen empl_agri = 0; 
replace empl_agri = empl_agri + e44*365  if e44 != . & e44 >= 0 ; * for permanent workers, we count them as working full day. Hence we take number of workers times 365 days; 
replace empl_agri = empl_agri + e46*e47_r if e46 != . & e46 >= 0 & e47_r != . & e47_r >= 0; 
replace empl_agri = empl_agri + e46 * e47_hr if e46 != . & e46 >= 0 & e47_hr != . & e47_hr >= 0; 

** Livestock Activities Employees; 
gen empl_livestock = 0 ; 
replace empl_livestock = empl_livestock + f22 * 365 if f22 !=. & f22 >= 0 ; 
replace empl_livestock = empl_livestock + f24 * f25 if f24 !=. & f24 >= 0 & f25 !=. & f25 >= 0 ; 

foreach var in empl_agri empl_livestock {;
	_pctile `var', p(99.7); 
	replace `var'= . if `var'> r(r1); 
	};

 
********************************************************************************************************
** Work of family members in own activities; 
**********************************************************************************************************; 

* First measure: total person-days per household last year; 
	* Unconditional; 

* Agricultural Work; 
gen work_agri = 0 ;  
foreach j of numlist 1(1)17 {; 
replace work_agri = work_agri + e42_`j' if e42_`j' !=. & e42_`j' >= 0 ; 
replace work_agri = work_agri + e43_`j' if e43_`j' !=. & e43_`j' >= 0 ; 
}; 

* Livestock work; 
* we want full work days, so we count full work day as 7 hours. We convert all work times into hours and divide by 7 to get person-days; 
gen work_livestock = 0 ; 
foreach j of numlist 1(1)17 {; 
replace work_livestock = work_livestock + f20_`j' * f21_`j' if f20_`j'!=. & f20_`j'  >= 0 & f21_`j' !=. & f21_`j' >= 0 ;  
}; 
replace work_livestock = work_livestock/7; 

foreach var in work_agri work_livestock {;
	_pctile `var', p(99.7); 
	replace `var'= . if `var'> r(r1); 
	};

***********************************;
** Weekly hours worked - past 7 days; 
***********************************; 
* We do not have data on this for wave 1 - baseline;
* Constructed for those aged between 6 and 65;

* We first identify questionnaires with no data for these questions;
	gen nonmiss=0;
		foreach j of numlist 1(1)20 {; 
			replace nonmiss = nonmiss + 1 if a12_m`j'!=. | a12_p`j'!=. | a13_`j'!=. | a14_`j'!=.  ;
			};

* We first create the groups we want to look at;

	foreach j of numlist 1(1)20 {; 
		gen age16_65`j' = (a7_`j' <=65 & a7_`j' >= 16);
		gen age16_20`j' = (a7_`j' <=20 & a7_`j' >=16);
		gen age20_50`j' = (a7_`j' <=50 & a7_`j' >20);
		gen age50_65`j' = (a7_`j' <=65 & a7_`j' >50);
			}; 

* Now, we create same set of variables for each group;
	
	foreach group in age16_65 age16_20 age20_50 age50_65 {; 

		* we compute whether it exists at least one member in the HH that belongs to a given group;
		gen member_`group'=0;
		foreach j of numlist 1(1)20 {; 
			replace member_`group' = 1 if `group'`j'==1;
			};

		*WARNING: do not change the following order in which variables are created - we later compute weekly hours worked per member based on this order;  

		foreach act in all self outside chores {;
		foreach j of numlist 1(1)20 {;
			gen hours_`act'_`group'`j' = 0 if `group'`j'==1 ; 
			};
			};
		};

foreach group in age16_65 age16_20 age20_50 age50_65 {; 
foreach j of numlist 1(1)20 {;
	replace hours_self_`group'`j' = hours_self_`group'`j'+ a12_p`j' if (a12_p`j' > 0 & a12_p`j' < 126 ) & wave != 1;
	replace hours_self_`group'`j' = hours_self_`group'`j'+ a12_m`j' if (a12_m`j' > 0 & a12_m`j' < 126 ) & wave != 1;
	replace hours_outside_`group'`j' = hours_outside_`group'`j' + a14_`j' if (a14_`j' > 0 & a14_`j' < 126 ) & wave != 1;
	replace hours_chores_`group'`j' = hours_chores_`group'`j' + a13_`j' if (a13_`j' > 0 & a13_`j' < 126 ) & wave != 1;

	replace hours_all_`group'`j' = hours_self_`group'`j' + hours_outside_`group'`j' + hours_chores_`group'`j' if wave != 1 ;

	}; 
	}; 



* We replace by missing time worked higher than 24*7=168 hours;
foreach group in age16_65 age16_20 age20_50 age50_65 {; 
foreach j of numlist 1(1)20 {; 
foreach act in self outside chores {;
		replace hours_`act'_`group'`j' =. if hours_all_age16_65`j' >168 & hours_all_age16_65`j'!=.;
		};
		replace hours_all_`group'`j' =. if hours_all_age16_65`j' >168 & hours_all_age16_65`j'!=.;
		};
		};

* We now compute the SUM of hours, to get the total number of hours worked by HH members in a week; 
	foreach group in age16_65 age16_20 age20_50 age50_65 {; 
	foreach act in all self outside chores {;
			egen hours_`act'_`group'=rsum(hours_`act'_`group'1-hours_`act'_`group'20);
			replace hours_`act'_`group'=. if nonmiss == 0; 
				label var hours_`act'_`group' "BL total hours worked in `act' by hh members `group'";  
			};
			};

* We put as missing group variables when the total is missing;
	foreach group in age16_65 age16_20 age20_50 age50_65 {; 
	foreach act in all self outside chores {;
		replace hours_`act'_`group'=. if hours_all_age16_65==.;
		};
		};
* Wave 1; 
* We do not have info on hours worked over the past 7 days, therefore we replace by missing;
	foreach group in age16_65 age16_20 age20_50 age50_65 {; 
	foreach act in all self outside chores {;
		gen hours_`act'_`group'_d = 0 ;
			label var hours_`act'_`group'_d "BL 1 if hours worked in `act' by hh members `group' missing";
		replace hours_`act'_`group' = . if wave ==1 ;
		replace hours_`act'_`group'_d = 1 if wave ==1 ;
		};
		};

	sum hours_all_age16_65- hours_chores_age16_65;
	drop nonmiss-age50_6520 member_age16_65- hours_chores_age16_6520  member_age16_20- hours_chores_age50_6520;

********************************************************************************************************
** Women's empowerment
********************************************************************************************************; 
* Household activities managed by women;
*****************************************;
gen women_act_number = 0;
	foreach j of numlist 1(1)6 {;
	foreach i of numlist 1(1)20 {;
		replace women_act_number = women_act_number + 1 if d3_`j' == `i' & a4_`i' == 2;
		};
		};

gen women_act = (women_act_number > 0);


********************************************************************************************************
********************************************************************************************************
** 2.INTERACTIONS MECHANISMS CONTROL VARIABLES
********************************************************************************************************
********************************************************************************************************

********************************************************************************************************
** Household Composition
********************************************************************************************************; 

gen head_male = 0; 

foreach j of numlist 1(1)20 {; 
replace head_male = 1 if a3_`j' == 1 & a4_`j' == 1; 
}; 

gen head_age = .; 
foreach j of numlist 1(1)20 {; 
replace head_age = a7_`j' if a3_`j' == 1 & a7_`j' != . & a7_`j' >= 0; 
}; 
gen head_age_d=0;

** Dummies for household head education; 
foreach i of numlist 1(1)16 {; 
gen head_educ_`i' = 0; 
}; 

foreach i of numlist 1(1)16 {; 
foreach j of numlist 1(1)16 {; 
replace head_educ_`i' = 1 if a3_`j' == 1 & a8_`j' == `i'; 
}; 
}; 

********************************************************************************************************
** Location
********************************************************************************************************; 

recode b3_1 b3_2 b3_3 b4_1_1 b4_1_1 b4_1_1 b4_1_1 b4_1_1 b4_1_1 (-99=.) (-98=.) (-97=.);

** Distance to the closest souk in km;
egen distance_soukkm = rmin(b3_1 b3_2 b3_3); 

_pctile distance_soukkm, p(99.9);
replace distance_soukkm=. if distance_soukkm> r(r1); 

** in minutes, with public transportation; 
egen distance_soukpt = rmin(b4_1_1 b4_1_2 b4_1_3); 
	replace distance_soukpt = . if wave == 1;

_pctile distance_soukpt, p(99.9);
replace distance_soukpt=. if distance_soukpt> r(r1); 
 
** in minutes, without public transportation; 
egen distance_soukmin = rmin(b4_2_1 b4_2_2 b4_2_3);
	replace distance_soukmin = . if wave == 1; 

_pctile distance_soukmin, p(99.9);
replace distance_soukmin=. if distance_soukmin> r(r1); 


*****************************;
*** Shocks;
*****************************;
gen shock1=0;
replace shock1=1 if (f29_1==1|f29_2==1|f29_3==1|e50_1==1|e50_2==1);

gen shock1agri=0;
replace shock1agri=1 if (e50_1==1|e50_2==1);

gen shock1live=0;
replace shock1live=1 if (f29_1==1|f29_2==1|f29_3==1);

gen shock2=0;
replace shock2=1 if (h6_5>0 & h6_5!=.)|(h6_6>0 & h6_6!=.)|(h6_7>0 & h6_7!=.);


*****************************************************************************************************;
*****************************************************************************************************;
* WE LABEL OUTCOMES; 
**********************;

*Loans;
label var aloans_alamana "BL number of outstanding loans: alamana"; 
label var aloans_oformal "BL number of outstanding loans: other formal source"; 
label var aloans_informal "BL number of outstanding loans: informal source"; 
label var aloans_branching "BL number of outstanding loans: utility company";
label var aloans_oamc "BL number of outstanding loans: other mfi";
label var aloans_total "BL number of outstanding loans: total";
label var aloans_oformal2 "BL number of outstanding loans: other formal source"; 
            
label var aloansamt_alamana "BL amount in MAD of outstanding loans: alamana"; 
label var aloansamt_oformal "BL amount in MAD of outstanding loans: other formal source"; 
label var aloansamt_informal "BL amount in MAD of outstanding loans: informal source";
label var aloansamt_branching "BL amount in MAD of outstanding loans: utility company";
label var aloansamt_oamc "BL amount in MAD of outstanding loans: other mfi";
label var aloansamt_oformal2 "BL amount in MAD of outstanding loans: other formal source";
label var aloansamt_total "BL amount in MAD of outstanding loans: total";  

label var ploans_alamana "BL number of loans that matured during the past 12 months: alamana"; 
label var ploans_oformal  "BL number of loans that matured during the past 12 months: other formal source"; 
label var ploans_informal "BL number of loans that matured during the past 12 months: informal source"; 
label var ploans_branching "BL number of loans that matured during the past 12 months: utility company";
label var ploans_oamc "BL number of loans that matured during the past 12 months: other mfi";
label var ploans_total "BL number of loans that matured during the past 12 months: total";
label var ploans_oformal2  "BL number of loans that matured during the past 12 months: other formal source"; 
            
label var ploansamt_alamana "BL amount (in MAD) of loans that matured during the past 12 months: alamana"; 
label var ploansamt_oformal "BL amount (in MAD) of loans that matured during the past 12 months: other formal source"; 
label var ploansamt_informal "BL amount (in MAD) of loans that matured during the past 12 months: informal source";
label var ploansamt_branching "BL amount (in MAD) of loans that matured during the past 12 months: utility company";
label var ploansamt_oamc "BL amount (in MAD) of loans that matured during the past 12 months: other mfi";
label var ploansamt_total "BL amount (in MAD) of loans that matured during the past 12 months: total";  
label var ploansamt_oformal2 "BL amount (in MAD) of loans that matured during the past 12 months: other formal source"; 

label var loans_alamana "BL number of loans: alamana"; 
label var loans_oformal  "BL number of loans: other formal source"; 
label var loans_informal "BL number of loans: informal source"; 
label var loans_branching "BL number of loans: utility company";
label var loans_oamc     "BL number of loans: other mfi";
label var loans_total    "BL number of loans: total";
label var loans_oformal2  "BL number of loans: other formal source"; 

label var loansamt_alamana "BL amount (in MAD) of loans: alamana"; 
label var loansamt_oformal "BL amount (in MAD) of loans: other formal source"; 
label var loansamt_informal "BL amount (in MAD) of loans: informal source";
label var loansamt_branching "BL amount (in MAD) of loans: utility company";
label var loansamt_oamc  "BL amount (in MAD) of loans: other mfi";
label var loansamt_total "BL amount (in MAD) of loans: total";  
label var loansamt_oformal2 "BL amount (in MAD) of loans: other formal source"; 

label var borrowed_alamana "BL 1 if borrowed from alamana";               
label var borrowed_oformal "BL 1 if borrowed from other formal source";                  
label var borrowed_informal "BL 1 if borrowed from informal source";     
label var borrowed_branching "BL 1 if borrowed from utility company";          
label var borrowed_oamc "BL 1 if borrowed from other mfi";
label var borrowed_total "BL 1 if borrowed from any source";      
label var borrowed_oformal2 "BL 1 if borrowed from other formal source";

label var nb_pasbesoin "BL 1 if reason loan not requested: does not need it";
label var nb_peurrejet "BL 1 if reason loan not requested: fear of being rejected"; 
label var nb_frais "BL 1 if reason loan not requested: high fees";
label var nb_taux "BL 1 if reason loan not requested: high interest rate";           
label var nb_distance "BL 1 if reason loan not requested: branches far away";         
label var nb_montantinsuf "BL 1 if reason loan not requested: amounts offered are small";          
label var nb_peurremboursement "BL 1 if reason loan not requested: fear of not being able to reimburse"; 
label var nb_peurpertegarantie "BL 1 if reason loan not requested: fear of loosing collateral"; 
label var nb_religion "BL 1 if reason loan not requested: religious reasons";
label var nb_pasgarantie "BL 1 if reason loan not requested: had no collateral"; 

label var credit_refused  "BL 1 if loan request rejected during the past 12 months"; 
label var credit_notasked "BL 1 if loan need during the past 12 month but not requested"; 
label var credit_need "BL 1 if current loan need"; 
label var try_credit "BL 1 if requested a loan during the past 12 months";

label var need_fin "BL share of own activities: need financing ";
label var need_form "BL share of own activities: need training ";
label var need_comm "BL share of own activities: need marketing ";                

* Self-employment activities;
label var inv_agri "BL agriculture: purchases of assets past 12 months (in MAD)";
label var asset_agri "BL agriculture: current stock of assets (in MAD)"; 
label var land_ha "BL surface area of land owned (in hectars)";
label var land "BL 1 if owns land";
label var land_expl_ha "BL superficie of land exploited (in hectars)";

label var inv_livestockanim "BL animal husbandry: purchases of livestock past 12 months (in MAD)";
label var inv_livestockmat "BL animal husbandry: purchases of equipment past 12 months (in MAD)";
label var inv_livestock "BL animal husbandry: purchases of assets past 12 months (in MAD)";
label var asset_livestock "BL animal husbandry: current stock of assets (in MAD)";

label var inv_business_assets "BL Non-agricultural business: purchases of assets past 12 months (in MAD)";   
label var asset_business "BL Non-agricultural business: current stock of assets (in MAD)"; 
label var inv_business_inputs "BL Non-agricultural business: purchases of inputs past 12 months (in MAD)";

label var act_agri "BL 1 if declared agricultural self-employment activity";
label var act_livestock "BL 1 if declared animal husbandry self-employment activity";
label var act_business "BL 1 if declared non-agricultural self-employment activity";

label var act_number "BL number of declared self-employment activities";
label var act_total_agri "BL number of types of agricultural self-employment activities";
label var act_total_live "BL number of types of animal husbandry self-employment activities";
label var act_number_business "BL number of non-agricultural self-employment activities";

label var selfempl_agri "BL 1 if agricultural self-employment activity";
label var selfempl_livestock "BL 1 if animal husbandry self-employment activity";
label var selfempl_business "BL 1 if non-agricultural self-employment activity";
label var self_empl "BL 1 if self-employment activity";

label var sale_cereal "BL sales and self-consumption of cereals past 12 months (in MAD)";
label var sale_tree "BL sales and self-consumption of tree fruits past 12 months (in MAD)";
label var sale_veg "BL sales and self-consumption of vegetables past 12 months (in MAD)";
label var sale_agri "BL sales and self-consumption of agriculture past 12 months (in MAD)"; 

label var sale_livestockanim "BL sales and self-consumption livestock animals past 12 months (in MAD)";    
label var sale_livestockprod "BL sales and self-consumption livestock products past 12 months (in MAD)";
label var sale_livestock "BL sales and self-consumption animal husbandry past 12 months (in MAD)";

label var sale_business "BL sales and self-consumption non-agricultural business past 12 months (in MAD)";
 
label var expense_agriinputs "BL agriculture: expenses in inputs past 12 months (in MAD)"; 
label var expense_agrilabor "BL agriculture: expenses in labor past 12 months (in MAD)"; 
label var expense_agriinv "BL agriculture: expenses in investment past 12 months (in MAD)"; 
label var expense_agrirent "BL agriculture: expenses in rent past 12 months (in MAD)"; 
label var expense_agri "BL agriculture: total expenses past 12 months (in MAD)";              

label var expense_livestockmatrent "BL animal husbandry: expenses in rent past 12 months (in MAD)"; 
label var expense_livestockmatinv "BL animal husbandry: expenses in equipment investment past 12 months (in MAD)";  
label var expense_livestockaniminv "BL animal husbandry: expenses in animal investment past 12 months (in MAD)"; 
label var expense_livestocklabor "BL animal husbandry: expenses in labor past 12 months (in MAD)"; 
label var expense_livestockinputs "BL animal husbandry: expenses in inputs past 12 months (in MAD)"; 
label var expense_livestock "BL animal husbandry: total expenses past 12 months (in MAD)";

label var expense_busrent "BL non-agricultural business: expenses in rent past 12 months (in MAD)";
label var expense_businv "BL non-agricultural business: expenses in investment past 12 months (in MAD)"; 
label var expense_businputs "BL non-agricultural business: expenses in inputs past 12 months (in MAD)";
label var expense_buslabor "BL non-agricultural business: expenses in labor past 12 months (in MAD)";
label var expense_business "BL non-agricultural business: total expenses past 12 months (in MAD)";

label var savings_cereal "BL cereals: current stock (in MAD)";
label var savings_tree "BL tree fruits: current stock (in MAD)";
label var savings_veg "BL vegetables: current stock (in MAD)";
label var savings_agri "BL agriculture: current stock (in MAD)";
label var savings_livestock "BL livestock: current stock(in MAD)"; 
label var savings_livestock_prod "BL livestock products: current stock (in MAD)";
label var savings_business "BL non-agricultural business: current stock (in MAD)";

label var prod_agri "BL agriculture output past 12 months: sales, self-consumption and current stock (in MAD)";  
label var stock_agri "BL agriculture: assets and current output stock ";
label var astock_livestock "BL animal husbandry: assets and livestock stock (in MAD)";
label var output_total "BL total output from self-employment activities past 12 months";

label var assets_total "BL total current stock of assets of self-employment activities, including livestock stock (in MAD)";
label var inv_total "BL total purchases of assets self-employment activitites past 12 months (in MAD)";
label var expense_total "BL total expenses self-employment activitites past 12 months (in MAD)";

label var profit_agri "BL agriculture: profit past 12 months (in MAD)";
label var profit_livestock "BL animal husbandry: profit past 12 months (in MAD)";
label var profit_business "BL non-agricultural business: profit past 12 months (in MAD)";
label var profit_total "BL self-employment activities: total profit past 12 months (in MAD)";

label var income_agri "BL income from agriculture past 12 months (in MAD)";
label var income_livestock "BL income from animal husbandry past 12 months (in MAD)";
label var income_business "BL income from non-agricultural business past 12 months (in MAD)";
label var income_dep "BL income from day labor and salaried past 12 months (in MAD)"; 
label var daily_dum "BL 1 if has a self-employment activity";
label var income_remittance "BL income from remittances past 12 months (in MAD)"; 
label var income_other "BL income from pension, asset sales, goverment and other source past 12 month (in MAD)";
label var income_pension "BL income from retirement and pension past 12 months (in MAD)";
label var income_assetsales "BL income from asset sales past 12 months (in MAD)"; 
label var income_gvt "BL income from gov program past 12 months (in MAD)";
label var income "BL total income past 12 months (in MAD)";

label var income_rest "BL income from other source past 12 months (in mad)"; 
label var income_self "BL income from self-employment activities past 12 months (in mad)";  
label var share_inc_self "BL share of income from self-employment activities to total income"; 
label var share_inc_dep "BL share of income from day labor and salaried to total income"; 
label var share_inc_rest "BL share of other income to total income";
label var share_cons_income  "BL share of expenses to total income";


label var lost_livestockanim "BL livestock lost past 12 months (in MAD)"; 
label var hadlost_livestockanim "BL 1 if lost livestock past 12 months"; 

* Shocks;
label var shock1 "BL 1 if shock to agriculture or animal husbandry production";
label var shock1agri "BL 1 if prevented to use more than half of the land or lost more than half of the harvest";
label var shock1live "BL 1 if lost more than half of livestock";
label var shock2 "BL 1 if health or house damage incident"; 

* Consumption;
label var cons_food "BL monthly food consumption (in MAD)";
label var cons_food_usual "BL monthly usual food consumption (in MAD)";
label var cons_durables "BL monthly expenditure on durables (in MAD)";
label var cons_nondurables "BL monthly expenditure on non-durables (in MAD)";
label var cons_repay "BL monthly loan repayments (in MAD)";
label var consumption "BL total monthly consumption (in MAD)";

* Per capita consumption;
label var members "BL number of people listes in hh roster";       
label var members_resid "BL number of hh members";
label var nadults_resid "BL number of members 16 years old or older";
label var nchildren_resid "BL number of membres younger than 16 years old";

label var conspc_food "BL monthly food per capita consumption (in MAD)";
label var conspc_nondurables "BL monthly per capita expenditure on non-durables (in MAD)";
label var conspc_durables "BL monthly per capita expenditure on durables (in MAD)";
label var consumption_pc "BL total monthly per capita consumption (in MAD)";
label var poor "BL 1 if household is poor";      
label var assetindex "BL index of home durable assets";

* Labor self-employment activities;
label var empl_agri "BL agriculture: days worked by employees past 12 months";          
label var empl_livestock "BL animal husbandry: days worked by employees past 12 months";          

label var work_agri "BL agriculture: days worked by members past 12 months";           
label var work_livestock "BL animal husbandry: days worked by members past 12 months";           

* Labor hh members past 7 days;
* with var creation;

* Social; 
label var women_act_number "BL number of self-employment activities managed by women"; 
label var women_act "BL 1 if self-employment activity managed by women"; 

* Controls;
label var head_male "BL 1 if male head"; 
label var head_age "BL head age";
label var head_age_d "BL if head age missing";

label var head_educ_1 "BL 1 if educational attainment: none";                 
label var head_educ_2 "BL 1 if educational attainment: koranic";                
label var head_educ_3 "BL 1 if educational attainment: 1st grade";
label var head_educ_4 "BL 1 if educational attainment: 2nd grade";
label var head_educ_5 "BL 1 if educational attainment: 3rd grade";
label var head_educ_6 "BL 1 if educational attainment: 4th grade";
label var head_educ_7 "BL 1 if educational attainment: 5th grade";
label var head_educ_8 "BL 1 if educational attainment: 6th grade";
label var head_educ_9 "BL 1 if educational attainment: 7th grade";
label var head_educ_10 "BL 1 if educational attainment: 8th grade";
label var head_educ_11 "BL 1 if educational attainment: 9th grade";
label var head_educ_12 "BL 1 if educational attainment: 10th grade";
label var head_educ_13 "BL 1 if educational attainment: 11th grade";
label var head_educ_14 "BL 1 if educational attainment: 12th grade";
label var head_educ_15 "BL 1 if educational attainment: higher educ/university";
label var head_educ_16 "BL 1 if educational attainment: professional training";

label var distance_soukkm "BL distance to souk (in kilometers)";                  
label var distance_soukpt "BL distance to souk by public transportation (in minutes)";                  
label var distance_soukmin "BL distance to souk without public transportation (in minutes)";

*******************************************************;
* WE SAVE BASELINE DATASET + OUTCOMES;

sort ident;
save Output\baseline_minienquete_outcomes.dta, replace; 

