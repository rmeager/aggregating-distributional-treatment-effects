* Removes the missing values from the Crepon data *

use "\\bbking2\rmeager\winprofile\mydocs\Research work\bayesian meta analysis\microcredit rct data\crepon et al 2015\Data&Code_AEJApp_MicrocreditMorocco\Input\Microcredit_EL_mini_anonym_processed_stata12_RMedit.dta", clear

drop if expense_total_m ==0
drop if sale_agri_m == 0 
drop if sale_livestock_m == 0 
drop if profit_total_m == 0 
drop if assets_total_m == 0 
drop if consumption_m == 0 
drop if borrowed_alamana_m == 0 
drop if savings_livestock_m == 0 
drop if savings_agri_m == 0 

saveold "\\bbking2\rmeager\winprofile\mydocs\Research work\bayesian meta analysis\microcredit rct data\crepon et al 2015\Data&Code_AEJApp_MicrocreditMorocco\Input\Microcredit_EL_mini_anonym_processed_stata12_RMedit.dta", replace
