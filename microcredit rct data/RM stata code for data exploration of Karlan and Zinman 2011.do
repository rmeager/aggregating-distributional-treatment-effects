* This code explores some discrepancies in the data set online for Karlan & Zinman 2010 *
* Written by Rachael Meager in January 2015 * 
set more off
* Karlan & Zinman (2011) says that 2158 people were studied, of which 1601 met the criteria for the randomisation, and 1113 could be found in follow up
* However, as the following command shows, we have 1978 individual observations (1975 unique) in the follow up data set:
codebook fu_survey_id

* Moreover this variable apparently claims that ALL of them had their loan decisions randomised: 

codebook css_randomizetag

* This variable apparently claims that all except 1 had loan decisions randomised:
 codebook css_rescuevar

* This must be incorrect since only 1601 were supposed to be eligible! 

* Moreover, some of them do not fall in the right range of credit scores (31-59), as the range here shows:

 codebook css_creditscorefinal

* The paper says that of the 1601 loans there are 1583 new loans but that is not what I see here:

codebook css_loantype


* Also, 90% of the css_loanrecommend variable is missing

codebook css_loanrecommend
 
* When we restrict to "sample  = 1" we have only 1113 observations

* But they are still not in the right credit score range:

codebook css_creditscorefinal if sample == 1

* Although most of the loan size recommendations are still missing
codebook css_loanrecommend if sample == 1

* Now we know that one branch had to be removed because the loan officers defied the randomisation
* The largest branch, and only one capable of leaving a sample size close to 1601 if removed, is GMA
* We also know it must be GMA as this is stripped out when the variable "sample" == 1
* We remove GMA and now have 1609 observations which is close to 1601 but not quite right

codebook fu_survey_id if css_branchid != 6

* But even removing them we do not get the right range for credit score
codebook css_creditscorefinal if css_branchid != 6

* removing GMA we still don't get the right number of new loans
codebook css_loantype if css_branchid != 6

* My Questions:
* 1. Why does this data seem to say that some people with ineligible credit scores got randomised? 
* 2. Is there a way I can recover the noncompliers in this data? I don't see how.

