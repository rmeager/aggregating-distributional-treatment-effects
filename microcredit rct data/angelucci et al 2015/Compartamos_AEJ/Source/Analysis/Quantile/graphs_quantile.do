/*
Authors:
	Andrew Hillis, Innovations for Poverty Action
	Matt White, Innovations for Poverty Action, mwhite@poverty-action.org
Date of creation: 2012
Purpose: Create the QTE graphs.
*/

vers 12.1


/* -------------------------------------------------------------------------- */
					/* initialize			*/

* Set the working directory.
cap c comprado

include header

enumtypes

cap log close inferenceplot
log using "${logf}inferenceplot", name(inferenceplot) s replace


/* -------------------------------------------------------------------------- */
					/* specify parameters	*/

* The paper for which to produce the results
loc paper `PaperAEJ'
* numlist of the quantiles
loc quants 5(5)95
* Maximum percentile shown on graphs:
* 100 is the maximum; 99 means 99th percentile.
loc maxg 95
* Minimum percentile shown on graphs
loc ming 5

* The outcome categories to run
* AEJ
loc categories `categories' `CatSelfEmploy'
loc categories `categories' `CatIncome'
loc categories `categories' `CatLabor'
loc categories `categories' `CatConsumptionChimera'
loc categories `categories' `CatSocialChimera'
loc categories `categories' `CatWelfareChimera'


/* -------------------------------------------------------------------------- */
					/* check parameters		*/

* `paper'
evalname Paper `paper'
assert inlist(`paper', `PaperAEJ')
* `quants'
numlist "`quants'"
loc quants `r(numlist)'
* `maxg', `ming'
foreach n in "`maxg'" "`ming'" {
	conf n `n'
	assert `n' == floor(`n')
	assert inrange(`n', 0, 100)
}
assert `maxg' > `ming'


/* -------------------------------------------------------------------------- */
					/* prepare dataset		*/

* Prepare a dataset for the graphing command (-eclplot-).

clear
loc vers 13
loc dir "$resdir/Datasets/Quantile subsets/Stata `vers', reps=1000"
foreach category of loc categories {
	evalname Cat `category'
	loc files : dir "`dir'" file "quantiles `=strlower("`r(name)'")' *.dta"
	foreach file of loc files {
		append using "`dir'/`file'"
	}
}

* Drop outdated specifications.
drop if inlist(outvar, "Q6_4_amount", "Q16_1_OtherDecidesTotal_cond")

gen typed_full = typed_type2 + cond(mi(typed_type2), "", "_") + typed_depvar
assert typed_full == outvar

* Replace outvar, the typed outcome name, with the more user-friendly
* typed_depvar, the dependent variable name. outvar is already equal to
* typed_depvar in the count variable results dataset.
bys outvar (typed_depvar): assert typed_depvar[1] == typed_depvar[_N]
bys typed_depvar (outvar): assert outvar[1] == outvar[_N]
replace outvar = typed_depvar

* Now that we are running the quantile regressions in pieces, creating results
* dataset subsets, ord does not identify the correct outcome order. It is
* recreated below.
drop ord

ds typed_type2, not
if "`r(varlist)'" != "" ///
	destring `r(varlist)', replace

* Relabel outcome categories.
foreach area in Credit Business Income Consumption "Other Welfare" {
	replace area = "`area'" if strpos(area, "`area'")
}

* Create variables for p-values and confidence intervals.
foreach i of loc quants {
	ren (q`i'_tc q`i'_tse) (tc_q`i' tse_q`i')

	gen p_q`i' = 2 * (1 - normal(abs(tc_q`i' / tse_q`i')))
	gen inference_q`i' = cond(p_q`i' < .1, "Significant", "Not significant")

	gen radius = abs(invnormal(.05)) * tse_q`i'
	gen CU_q`i' = tc_q`i' + radius
	gen CL_q`i' = tc_q`i' - radius
	drop radius
}

* Reshape so that each observation is a set of results for an individual
* variable/quantile.
reshape long tc_q tse_q p_q inference_q CU_q CL_q, i(outvar) j(quantile)
assert !mi(outvar, olab)

* Append the count variable results.

* Variables only in the quantile results dataset
loc resonly typed_full category area reps edp_tab edp_ksm
* Variables only in the count variable results dataset
loc countonly var_val var_val_o
* Shared variables that differ and will be dropped
loc drop df
* Shared variables that are constant within typed_depvar
loc cons outvar olab typed_type2
* Shared variables for which differences are acceptable
loc diffok quantile tc_q tse_q p_q inference_q CU_q CL_q

loc shared `drop' `cons' `diffok'
conf var `shared', exact
conf new var `countonly'

preserve

u "$resdir/Datasets/quantiles_counts", clear

drop N
conf var `shared', exact
conf new var `resonly'

tempfile countdta
sa `countdta'

restore

append using "`countdta'", gen(count_var)

unab all : _all
loc expected typed_depvar count_var `resonly' `countonly' `shared'
loc all : list sort all
loc expected : list sort expected
di "{p}`all'{p_end}"
di "{p}`expected'{p_end}"
assert `:list all === expected'

* Drop count-only results.
bys typed_depvar (count_var): drop if count_var[1]

drop `drop'

* Check variables that should be constant by typed_depvar.
foreach var of loc cons {
	bys typed_depvar (`var'): assert `var'[1] == `var'[_N]
}
assert typed_depvar == outvar
bys olab (outvar): assert outvar[1] == outvar[_N]

* Update the count variable results with the quantile results.
foreach var of loc resonly {
	bys typed_depvar count_var (`var'): ///
		assert `var'[1] == `var'[_N] if !count_var
	bys typed_depvar (count_var): replace `var' = `var'[1]
}

* Drop duplicates.
gsort olab -count_var
bys olab (count_var): gen dup = count_var[_N]
drop if dup & !count_var
drop dup

* Create order variables.
* ord
gen ord = .
loc ord 0
foreach category of loc categories {
	do "Analysis/Quantile/Quantile outcome group" category(`category') group(g0)
	foreach typed of loc g0 {
		replace ord = `++ord' if typed_full == "`typed'"
	}
}
assert !mi(ord)
* order
sort quantile
gen order = _n
* List of order variables
loc order ord order

tempfile qtes
sa `qtes'


/* -------------------------------------------------------------------------- */
					/* outcome distributions	*/

* Add variables about the outcomes' distributions.

tempname post
tempfile res
postfile `post' str`c(namelen)' typed_depvar quantile Treatment val_ using `res'

sampexp `SampleEndline'
loc endline `r(exp)'
levelsof typed_full if !count_var
foreach typed in `r(levels)' {
	parse_typed_outcome `typed'
	loc depvar `s(depvar)'
	assert "`depvar'" != "Q10_4_tot_helpers_some"

	do "Analysis/Quantile/Prepare regression variables" ///
		typed(`typed') paper(`paper') sampcode(`SampleEndline') dopanel(dopanel)
	if "`typed'" != "`depvar'" {
		drop `depvar'
		ren `typed' `depvar'
	}
	keep if `endline' & !mi(`depvar')

	forv i = 0/1 {
		qui centile `depvar' if `endline' & Treatment == `i', c(`ming'/`maxg')
		assert r(n_cent) == `maxg' - `ming' + 1
		loc ctr 0
		forv j = `ming'/`maxg' {
			post `post' ("`depvar'") (`j') (`i') (r(c_`++ctr'))
		}
	}
}

postclose `post'

u `res', clear

assert !mi(val_)

gen j = cond(Treatment, "t", "c")
drop Treatment
reshape wide val_, i(typed_depvar quantile) j(j) str

sa `res', replace

u `qtes', clear

merge 1:1 typed_depvar quantile using `res', update
assert (_merge == 1) + (count_var == 1) != 1
drop _merge

* Update new observations (_merge == 2 above).
foreach var in `resonly' `cons' `order' count_var {
	cap conf str var `var'
	loc sign = cond(!_rc, "-", "+")
	gsort typed_depvar `sign'`var'
	by typed_depvar: replace `var' = `var'[1]
}

* Check count_var.
assert inlist(count_var, 0, 1)
bys typed_depvar (count_var): assert count_var[1] == count_var[_N]

sa `qtes', replace


/* -------------------------------------------------------------------------- */
					/* graph				*/

graph drop _all

foreach category of loc categories {
	preserve
	u "$andata_aej" in 1, clear
	do "Analysis/Set category properties" ///
		spec(Full_TvC) cat(`category') sampcode(`SampleEndline') ///
		titles(titles)
	restore
	assert `:list sizeof titles' == 1
	gettoken title : titles
	assert regexm("`title'", "^Table ([1-9]): (.+)$")
	loc figure = regexs(1)
	loc tabletitle = regexs(2)

	levelsof olab if category == `category', loc(varlabs)
	mata: st_local("maxlen", ///
		strofreal(max(strlen(tokens(st_local("varlabs"))))))

	loc varctr 0
	levelsof ord if category == `category'
	foreach ord in `r(levels)' {
	/* ---------------------------------------------------------------------- */
					/* -eclplot- of outcome		*/

		loc ++varctr

		u `qtes', clear

		* Typed outcome name
		levelsof typed_full if ord == `ord', loc(typed)
		assert `:list sizeof typed' == 1
		gettoken typed : typed

		* Outcome variable label
		levelsof olab if ord == `ord', loc(varlab)
		assert `:list sizeof varlab' == 1
		gettoken varlab : varlab

		* -eclplot()- options

		* -title()-
		mata: st_local("ascii_a", strofreal(ascii("a")))
		loc ltr = char(`ascii_a' + `varctr' - 1)
		if `paper' == `PaperAEJ' {
			loc len = cond(`maxlen' > 40, 37, `maxlen')
			pieces "`varlab'", len(`len')
			loc title "`s(pieces)'"
			gettoken first rest : title
			loc title "`"`figure'`ltr'. `first'"' `rest'"
		}

		* -t1title()-
		if `paper' == `PaperAEJ' {
			loc t1title
			#d ;
			loc units
				Thousands	1000
					"Q13_3_amount Q13_jobincome remitandtrans Q13_5_amount_mo
					Q9_totvalue"
				Hundreds	100
					"Q10_9_totrev Q10_8_totexp Q10_9_toprof
					Q7_nonfood_nondur foodconsump
					Q18_2_2_amount_wk Q18_2_1_amount_wk Q7_tempt Q18_2_3_amount_wk"
			;
			#d cr
			while `:list sizeof units' & !`:length loc t1title' {
				gettoken english	units : units
				gettoken rescale	units : units
				gettoken outcomes	units : units
				if `:list typed in outcomes' {
					loc t1title `english' of pesos
					foreach fv of var tc_q CU_q CL_q val_t val_c {
						replace `fv' = `fv' / `rescale' if ord == `ord'
					}
				}
			}

			if !`:length loc t1title' {
				preserve

				parse_typed_outcome `typed'
				assert "`s(types)'" == ""
				u `typed' using "$andata_aej" in 1, clear
				if "`:char `typed'[Unit]'" == "peso" ///
					loc t1title Pesos

				restore
			}
		}

		* -yscale()-, -ylabel()-
		loc bv
		loc tv
		su CU_q if ord == `ord'
		if r(max) > 0 ///
			loc bv = -.2 * r(max)
		if `paper' == `PaperAEJ' {
			loc fmt_q = cond(max(abs(r(min)), abs(r(max))) > 1, ///
				"%9.0f", "%9.2f")
		}
		su CL_q if ord == `ord'
		if r(min) < 0 ///
			loc tv = -.2 * r(min)

		levelsof count_var if ord == `ord', loc(count_var)
		assert `:list sizeof count_var' == 1
		if !`count_var' {
			loc x2

			foreach i in `ming' 10 20 30 40 50 60 70 80 90 `maxg' {
				loc lab
				levelsof val_c if ord == `ord' & quantile ==`i', local(lab) clean
				loc lab = string(`lab', "%9.1f")
				loc x2 `"`x2' `i' "`lab'""'
			}

			loc ytitle QTE
			loc xlabel `ming' 10 20 30 40 50 60 70 80 90 `maxg', labcolor(gs1) format(%9.0f)
			loc xscale_range
		}
		else {
			* store correct values for percentiles and values in the control group at that percentile
			levelsof quantile if ord == `ord' & count_var ==1, local(xlabs)
			levelsof var_val_o if ord == `ord' & count_var ==1, local(xvarval) clean

			loc wc: word count `xvarval'
			/*
			loc tvq: word 1 of `xvarval'
			if `tvq' == 1 loc xlabs 1 `xlabs'
			else loc xlabs 0 `xlabs'
			*/
			loc x2
			forvalues tr = 1/`wc' {
				loc qv: word `tr' of `xlabs'
				loc tvq: word `tr' of `xvarval'
				if `tr' !=`wc' loc x2 `x2' `qv' "`tvq'"
				else loc x2 `x2' `qv' "`tvq'"
			}

			loc ytitle AIT
			loc xlabel `xlabs', axis(1) labcolor(gs1) format(%9.0f)
			loc xscale_range range(5 95)
		}

		gen zero = 0
		#d ;
		eclplot tc_q CL_q CU_q quantile if ord == `ord', scale(.8) name(graph_`varctr', replace)
			/* titles */
			title(`title', size(large) margin() align(top) color(gs1))
			t1title("`t1title'", orient(horizontal))
			ytitle("`ytitle'", axis(1)) xtitle("", margin() color(gs1))
			/* y axis */
			yscale(range(`bv' `tv'))
			ylabel(#4, angle(horizontal) labcolor(gs1) axis(1) format("`fmt_q'"))
			yline(0, lpattern(shortdash) lw(thin) lc(gs6))
			/* x axis */
			xscale(lc(gs1) `xscale_range') xlabel(`xlabel' labsize(small))
			/* -estopts*()-, -ciopts*()- */
			supby(inference_q) legend(off)
			estopts(msym(O) mcolor(black) mlabcolor(gs3))
			estopts1(mfcolor(white))
			estopts2(mfcolor(black))
			ciopts(lc(gs1) lpat(dash))
			/* region */
			plotregion(style(none) color(white))
			graphregion(style(none) color(white))
			/* scatter */
			plot(scatter zero quantile,
				xaxis(2) connect(none) sort(quantile) msym(none none)
				xscale(axis(2) noline alt `xscale_range') xtitle("", axis(2)) xlabel(`x2', axis(2) noticks labcolor(gs1) format(%9.0f) labsize(small))
			)
		;
		#d cr
		drop zero

		window manage close graph

					/* -eclplot- of outcome		*/
	/* ---------------------------------------------------------------------- */
	}

	/* ---------------------------------------------------------------------- */
					/* -graph combine- of category	*/

	loc 1 1
	levelsof count_var if category == `category', loc(count_var)
	#d ;
	if `category' == `CatOldIncome' | !`:list 1 in count_var' {;
		loc Bcaption Vertical lines show 90% confidence intervals for
			quantile treatment effects with
			standard errors block-bootstrapped by cluster with
			1,000 replications.
		;
	};
	else {;
		loc Bcaption For continuous variables, vertical lines show
			90% confidence intervals for quantile treatment effects with
			standard errors block-bootstrapped by cluster with
			1,000 replications.
			For count variables, vertical lines show
			90% confidence intervals for the AIT estimate of
			the likelihood of treatment group respondents having
			the value on the x axis for that outcome relative to
			the control group respondents having that value.
			Standard errors are clustered by the unit of randomization.
			A + sign indicates that the value of the variable is at or above
			that number.
		;
	};
	if `category' == `CatOldBusiness' {;
		loc Bcaption `Bcaption' The sample for all estimates includes
			only business owners, except for the sample in Figure 2b, which
			includes only business owners with >0 employees.
		;
	};
	#d cr

	pieces `"`Bcaption'"', len(162)
	loc caption_l "`s(pieces)'"

	getlist graph_*, n(1/`varctr')
	graph combine `s(list)', col(2) name(graphcomb_`category') scale(.9) ///
		title("Figure `figure': Quantile Treatment Effects for `tabletitle'", size(small)) ///
		caption(`caption_l', size(vsmall)) imargin(vsmall) b1title("x axis shows the quantile (top row) and the control group value at that quantile (bottom row)", size(vsmall)) ///
		plotregion(style(none) color(white)) ///
		graphregion(style(none) color(white))

	evalname Paper `paper'
	loc papername `r(name)'
	loc paperstr " `papername'"
	cap mkdir "$resdir/Figures/QTEs/`papername'"
	loc base "$resdir/Figures/QTEs/`papername'/Figure `figure'`paperstr' - QTEs for `tabletitle'"
	graph export "`base'.pdf", replace
	graph export "`base'.ps", replace ///
		logo(off) pagesize(letter) mag(180) tmargin(.5) lmargin(.4) ///
		orientation(landscape)
	graph export "`base'.wmf", replace

	window manage close graph

					/* -graph combine- of category	*/
	/* ---------------------------------------------------------------------- */
}


/* -------------------------------------------------------------------------- */
					/* finish up			*/

log close inferenceplot
