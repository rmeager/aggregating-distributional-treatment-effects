/*
Authors:
	Andrew Hillis, Innovations for Poverty Action
	Matt White, Innovations for Poverty Action, mwhite@poverty-action.org
Purpose: Create Appendix Table 1.
Notes: This is an intermediate step in the creation of the final version of
Appendix Table 1 of the AEJ paper. Additional manual formatting is required:
1. Remove all text and numbers in columns 1-3 of the table. These are
	placeholders only.
2. Copy columns 4-6 of Table 1 as columns 1-3 of Appendix Table 1.
*/

vers 13


/* -------------------------------------------------------------------------- */
					/* initialize			*/

* Set the working directory.
cap c comprado

include header

enumtypes


/* -------------------------------------------------------------------------- */
					/* define parameters	*/

* Display format of real numbers in the table
loc format %24.3f

* Balance variables
do "Analysis/tables_descriptive/Define descriptive globals"
#d ;
loc balvars
	BTreatment
	collin1
	BF5
	collin2 collin3 collin4
	business_before
	BQ1_2_married_bin BQ1_2_sep_bin
	$descbasevars
;
#d cr

* Check that `balvars' matches $Baseline_vars.
foreach var of loc balvars {
	loc select `select' `=!strmatch("`var'", "collin*")'
}
loc copy `balvars'
mata: select_list("copy", "select")
assert `:list copy === global(Baseline_vars)'


/* -------------------------------------------------------------------------- */
					/* modify dataset		*/

u "$andata_aej", clear

* Create collinear variables so that Appendix Table 1 more closely matches Table
* 1.
forv i = 1/4 {
	gen collin`i' = 0
}
lab var collin1		Female
lab var collin2		"Primary school or none (omitted: above high school)"
lab var collin3		"Middle school"
lab var collin4		"High school"

do "Analysis/tables_descriptive/Descriptive variable labels"

tempfile descdata
sa `descdata'


/* -------------------------------------------------------------------------- */
					/* check parameters		*/

u `descdata', clear

* `format'
mata: assert(st_isnumfmt(st_local("format")))

* `balvars'
conf var `balvars', exact
unab unab : `balvars'
assert `:list balvars == unab'
loc BTreatment BTreatment
assert `:list BTreatment in balvars'


/* -------------------------------------------------------------------------- */
					/* balance tests		*/

u `descdata', clear

est clear

* Interactions with treatment
sampexp `SampleTargeted'
loc targeted "`r(exp)'"
foreach var of loc balvars {
	if !inlist("`var'", "Treatment", "BTreatment") & ///
		!strmatch("`var'", "collin*") {
		missgen BTX_`var' = BTreatment * `var', miss(`var')
		replace BTX_`var' = . if !`targeted'
		loc interact `interact' BTX_`var'
	}
}

assert survey != "Telephone" if `targeted'
assert mi(attrited) if survey == "Telephone"

loc i 0
foreach indep in BTreatment "`balvars'" "`balvars' `interact'" {
	reg surveyed `indep' if `targeted', cl(BCluster)

	* Check that there was no unexpected collinearity.
	loc indepvars : colnames e(b)
	loc temp : subinstr loc indepvars "o." "", all cou(loc o)
	loc temp : subinstr loc indepvars "o.collin" "", all cou(loc collin)
	assert `o' == `collin'

	loc hasinteract : list interact in indep
	estadd loc intg = cond(`hasinteract', "Yes", "No")

	su surveyed if e(sample)
	estadd loc ymean = strofreal(r(mean), "`format'")
	ta cluster if e(sample)
	estadd loc numclus = r(r)

	test `indep'
	estadd loc pint = strofreal(r(p), "`format'")

	est store b`++i'
}

est restore b3

test BTreatment `interact'
estadd loc intp = strofreal(r(p), "`format'")

* Check that none of the TX_* variables are significant. From the notes:
* "The coefficients on the interaction terms (not shown) are each not
* significant."
foreach var of loc interact {
	test `var'
	assert r(p) > .1
}

est store b3


/* -------------------------------------------------------------------------- */
					/* notes				*/

#d ;
loc notes Respondents are Mexican women aged 18-60 and all reside in outlying
	areas of Nogales. Column 2 reports the coefficient on treatment assignment
	(1=Treatment, 0=Control) when the variable in the row is regressed on
	treatment assignment. Column 3 reports the results of balance tests. The
	cells show the coefficient for each variable when they are all included in
	one regression with treatment assignment as the dependent variable. Column 4
	reports the coefficient on each variable in the row when they are all
	included in one regression with a binary variable for survey response
	(1=yes, 0=no) as the outcome variable. Column 5 reports the coefficient on
	treatment assignment when it is included in a regression with survey
	response as the outcome. Column 6 reports the results of the test for
	unbalanced attrition between treatment and control groups. The cells show
	the coefficient for each variable when they are all included in one
	regression along with each of their interactions with treatment, with survey
	response as the outcome. The coefficients on the interaction terms (not
	shown) are each not significant. Standard errors, clustered by the unit of
	randomization, are in parentheses below the coefficients.
	* p<0.10, ** p<0.05, *** p<.01.
;
#d cr
mata: st_local("notes", strtrim(stritrim(st_local("notes"))))


/* -------------------------------------------------------------------------- */
					/* -esttab-				*/

u `descdata', clear

lab var surveyed "Outcome:ASCII_10Not attrited"
lab var BQ1_2_married_bin "Married (omitted: single)"

tempfile temptable
#d ;
esttab b1 b1 b1 b2 b1 b3 using `temptable', tab replace
	nogap title("Appendix Table 1: Attrition")
	mgroups("Predicting Attrition", pattern(1 0 0))
	mlabel(, depvars) lab num
	drop(`interact' _cons) lz
	cells(
		b( label(" ") fmt("%24.3f") star)
		se(label(" ") fmt("%24.3f") par("(" ")"))
	)
	stats(scalar_title_blank N numclus pint intg ymean intp,
		fmt(%24.0g %24.0g %24.0g `format' `format') lab(
		" "
		N
		"Number of clusters"
		"pvalue of F test of joint significance of explanatory variables"
		"Above variables interacted with Treatment"
		"Outcome mean"
		"p-value from test that Treatment and all other variables above interacted with Treatment are jointly 0"
	))
	star(* 0.1 ** 0.05 *** .01) nonotes addnotes(`"`notes'"')
	sub("o." "" "(.)" "")
;
#d cr


/* -------------------------------------------------------------------------- */
					/* additional formatting	*/

import delim using "`temptable'", delim("\t") varn(nonames) stringc(_all) clear

ren v* c*

* Create variable row for the row number, using type double to avoid precision
* issues after some row numbers are modified to be non-integer.
gen double row = _n

* Switch the model numbers and labels rows.
loc anygroup 1
loc numbers = 2 + `anygroup'
loc labels  = 3 + `anygroup'
recode row (`numbers'=`labels') (`labels'=`numbers')
loc temp `numbers'
loc numbers `labels'
loc labels `temp'

#d ;
lab de table
	1	title
	2	groups
	3	labels
	4	numbers
	5	coeffs
	6	scalars
	7	notes
;
#d cr

su row if c1 == "Above variables interacted with Treatment"
assert r(N) == 1
loc startscalar = r(min)
assert `startscalar' >= 7 + `anygroup'
su row if c1 == "p-value from test that Treatment and all other variables above interacted with Treatment are jointly 0"
assert r(N) == 1
loc endscalar = r(min)
assert `endscalar' >= `startscalar'

gen type = .
lab val type table
replace type = "title":table	if row == 1
replace type = "groups":table	if row == 2 & `anygroup'
replace type = "labels":table	if row == `labels'
replace type = "numbers":table	if row == `numbers'
replace type = "coeffs":table ///
	if inrange(row, 4 + `anygroup', `startscalar' - 1)
replace type = "scalars":table	if inrange(row, `startscalar', `endscalar')
replace type = "notes":table	if row > `endscalar'
assert !mi(type)

reshape long c, i(row) j(col)
gen f = "size10"

* We will complete section-specific formatting, then general formatting.

* Format title.
replace f = f + " bold mergeright6" if type == "title":table & col == 1

* Drop the slashes row.
loc slashes = 4 + `anygroup'
drop if row == `slashes'

* Format groups.
replace f = f + " bold" if type == "groups":table & c != ""
* Remove "Group".
replace c = "" if type == "groups":table & col == 1
* Merge.
gsort row -col
gen merge = .
replace merge = cond(c[_n - 1] != "", 0, max(merge[_n - 1], 0)) + mi(c) ///
	if type == "groups":table
replace f = f + " mergeright" + strofreal(merge) ///
	if type == "groups":table & c != ""
drop merge
* Extra columns
su row
loc rows = r(max)
levelsof col if type == "groups":table & c != "", loc(cols)
gettoken col cols : cols
loc offset 0
foreach col of loc cols {
	loc offcol = `col' + `offset'
	loc groupcols : list groupcols | offcol
	expand 2 if col == `offcol', gen(copy)
	replace c = "" if copy
	replace f = "" if copy
	replace col = col + 1 if col >= `offcol' & !copy
	drop copy
	loc ++offset
}

* Format notes.
replace f = f + " mergeright6" if type == "notes":table & col == 1 & !mi(c)

* Alignment
replace f = f + " center" ///
	if !inlist(type, "title":table, "notes":table) & col > 1
replace f = f + " justify" if type == "notes":table

* Wrap
gen wraprow = !(type == "notes":table & mi(strtrim(c)) & col == 1)
bys row (wraprow): replace wraprow = wraprow[1]
replace f = f + " wrap" if wraprow
su col
loc maxcol = r(max)
replace f = f + " autofitrow" if wraprow & col == `maxcol'
drop wraprow

* Borders
su row if type == "scalars":table
replace f = f + " brbottomdouble" if type == "title":table | row == r(max)
replace f = f + " brbottom" ///
	if inlist(type, "labels":table, "numbers":table) & col > 1

* Concatenate format and column variables.
gen fc = "{" + strtrim(itrim(f)) + "}" + c

reshape wide c f fc, i(row) j(col)

/* Add row heights and column widths. Create variable rowheight for each row's
height, and add column widths to the topmost row (i.e., the row with the minimum
value of variable row). Once rowheight is -order-ed to the beginning of the
dataset and the dataset is sorted by variable row, the dataset will look like
this:

rowheight	c1	c2	c3	...
---------------------------
.			30	30	30	...		[column widths]
15			A1	B1	C1	...
15			A2	B2	C2	...
...			...	...	...	...

This means that in the table .csv file, the text and formatting of cell A1 won't
start in A1 but B2 (one row and column over). Similarly, the text and formatting
of B1 won't start in B1 -- that is where the width of column A is specified --
but instead in C2. After processing them, the formatTables macro removes the row
heights column and column widths row, returning all cells to their proper
locations. */

* Row heights
gen rowheight = .
order rowheight
egen nonmiss = rownonmiss(c*), str
replace rowheight = 15
//if !nonmiss | type != "coeffs":table | ///
//	type != "scalars":table
drop nonmiss
replace rowheight = 20 if type == "groups":table
su row if type == "notes":table
replace rowheight = 22 if row == r(min)
replace rowheight = 8 if type == "notes":table & mi(strtrim(c1))
replace rowheight = 15 * ceil(strlen(c1) / 31) if mi(rowheight)
replace rowheight = max(rowheight, 30) if type == "coeffs":table
assert !mi(rowheight)
* Column widths
set obs `=_N + 1'
su row
replace row = r(min) - 1 in L
foreach var of var fc* {
	replace `var' = "12" in L
}
replace fc1 = "24" in L
foreach col of loc groupcols {
	replace fc`col' = "0.5" in L
}
sort row

* Output table.
loc tablefile "$resdir/Tables/Unformatted/Appendix Table 1.csv"
export delim rowheight fc* using "`tablefile'", delim(",") novar replace
