/*
Author: Matt White, Innovations for Poverty Action, mwhite@poverty-action.org
Syntax: getlist pattern, n(numlist)
For a pattern containing exactly one asterisk (*), returns in s(list) a string
list of the strings that result when the * of pattern is replaced with each
element of the numlist specified to option -n()-. For instance,
-getlist Q1_*, n(1/5)- returns "Q1_1 Q1_2 Q1_3 Q1_4 Q1_5".
*/
pr getlist, sclass
	vers 9

	syntax anything(name=pattern id=pattern), n(numlist int >0 sort)

	* Check `pattern'.
	loc temp : subinstr loc pattern "*" "", all cou(loc stars)
	if `stars' != 1 {
		di as err "invalid pattern"
		ex 198
	}

	loc matapat st_local("pattern")
	mata: st_local("pos", strofreal(strpos(`matapat', "*")))
	mata: st_local("before", substr(`matapat', 1, `pos' - 1))
	mata: st_local("after",  substr(`matapat', `pos' + 1, .))
	mata: st_global("s(list)", invtokens( ///
		st_local("before") :+ tokens(st_local("n")) :+ st_local("after")))
end
