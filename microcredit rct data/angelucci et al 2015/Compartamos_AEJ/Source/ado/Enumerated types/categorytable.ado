* Author: Matt White, Innovations for Poverty Action, mwhite@poverty-action.org
* Syntax: categorytable category
* Returns the table name associated with category in r(name).
pr categorytable
	vers 9

	args category
	assert !`:length loc 2'
	conf integer n `category'

	enumtypes
	mata: st_global("r(name)", categorytable(`category'))
end

vers 9

loc RS	real scalar
loc SS	string scalar

enumtypes

mata:

`SS' categorytable(`RS' _category)
{
	`SS' table

	// AEJ
	if (_category == `CatCredit')
		_error("use chimera")
	else if (_category == `CatCreditChimera')
		table = "2a. " + evalname("Cat", _category)
	else if (_category == `CatCreditAmount')
		table = "2b. " + evalname("Cat", _category)
	else if (_category == `CatSelfEmploy')
		table = "3. " + evalname("Cat", _category)
	else if (_category == `CatIncome')
		table = "4. " + evalname("Cat", _category)
	else if (_category == `CatLabor')
		table = "5. " + evalname("Cat", _category)
	else if (_category == `CatConsumption')
		_error("use chimera")
	else if (_category == `CatConsumptionChimera')
		table = "6. " + evalname("Cat", _category)
	else if (_category == `CatSocial')
		_error("use chimera")
	else if (_category == `CatSocialChimera')
		table = "7. " + evalname("Cat", _category)
	else if (_category == `CatWelfare')
		_error("use chimera")
	else if (_category == `CatWelfareChimera')
		table = "8. " + evalname("Cat", _category)
	// Unknown
	else {
		_error(sprintf("unknown outcome category %f", _category))
	}

	return(table)
}

end
