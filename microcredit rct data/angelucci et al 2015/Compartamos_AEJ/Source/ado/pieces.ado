* Author: Matt White, Innovations for Poverty Action, mwhite@poverty-action.org
* Using the -:piece- extended macro function, break a string up into a list of
* pieces.
pr pieces, sclass
	vers 10.1

	syntax anything(name=s id=string), len(integer)

	gettoken s rest : s
	if `:length loc rest' {
		di as err "invalid string"
		ex 198
	}

	loc i 0
	loc piece " "
	while `:length loc piece' {
		loc piece : piece `++i' `len' of `"`s'"'
		if `:length loc piece' ///
			loc pieces "`pieces' `"`piece'"'"
	}
	loc pieces : list retok pieces

	sret loc pieces "`pieces'"
end
