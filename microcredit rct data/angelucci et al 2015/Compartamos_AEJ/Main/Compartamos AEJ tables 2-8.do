version 11.2

use if survey == "Endline" using data/analysis_data_AEJ_pub, clear

xi i.supercluster_xi
unab sc_controls : _Isuper*

* Table 2a: Credit Access
regress Q21_3_anymfi Treatment `sc_controls', vce(cl cluster)
regress in_admin Treatment `sc_controls', vce(cl cluster)
regress Q21_3_comp Treatment `sc_controls', vce(cl cluster)
regress Q21_3_othmfi Treatment `sc_controls', vce(cl cluster)
regress Q21_3_bank Treatment `sc_controls', vce(cl cluster)
regress Q21_3_othformal Treatment `sc_controls', vce(cl cluster)
regress Q21_3_informal2 Treatment BE_Q21_3_informal2 BE_Q21_3_informal2_mdum InPanel `sc_controls', vce(cl cluster)
regress Q21_3_oth Treatment BE_Q21_3_oth BE_Q21_3_oth_mdum InPanel `sc_controls', vce(cl cluster)
regress Q21_anyloan Treatment BE_Q21_anyloan BE_Q21_anyloan_mdum InPanel `sc_controls', vce(cl cluster)
regress A_ever_late_not_cond Treatment `sc_controls', vce(cl cluster)

* Table 2b: Loan Amounts
regress Q21_5_anymfi Treatment `sc_controls', vce(cl cluster)
regress Q21_5_comp Treatment `sc_controls', vce(cl cluster)
regress Q21_5_othmfi Treatment `sc_controls', vce(cl cluster)
regress Q21_5_bank Treatment `sc_controls', vce(cl cluster)
regress Q21_5_othformal Treatment `sc_controls', vce(cl cluster)
regress Q21_5_informal2 Treatment BE_Q21_5_informal2 BE_Q21_5_informal2_mdum InPanel `sc_controls', vce(cl cluster)
regress Q21_5_oth Treatment BE_Q21_5_oth BE_Q21_5_oth_mdum InPanel `sc_controls', vce(cl cluster)
regress Q21_5_all Treatment BE_Q21_5_all BE_Q21_5_all_mdum InPanel `sc_controls', vce(cl cluster)

* Table 3: Self-Employment Activities
regress Q10_9_totrev Treatment BE_Q10_9_totrev BE_Q10_9_totrev_mdum InPanel `sc_controls', vce(cl cluster)
regress Q10_8_totexp Treatment BE_Q10_8_totexp BE_Q10_8_totexp_mdum InPanel `sc_controls', vce(cl cluster)
regress Q10_9_toprof Treatment BE_Q10_9_toprof BE_Q10_9_toprof_mdum InPanel `sc_controls', vce(cl cluster)
regress Q10_any Treatment BE_Q10_any BE_Q10_any_mdum InPanel `sc_controls', vce(cl cluster)
regress Q10_1 Treatment BE_Q10_1 BE_Q10_1_mdum InPanel `sc_controls', vce(cl cluster)
regress bizstart12mo Treatment BE_bizstart12mo BE_bizstart12mo_mdum InPanel `sc_controls', vce(cl cluster)
regress bizclose Treatment BE_bizclose BE_bizclose_mdum InPanel `sc_controls', vce(cl cluster)

* Table 4: Income
regress Q13_3_amount Treatment BE_Q13_3_amount BE_Q13_3_amount_mdum InPanel `sc_controls', vce(cl cluster)
regress Q13_jobincome Treatment `sc_controls', vce(cl cluster)
regress remitandtrans Treatment `sc_controls', vce(cl cluster)
regress Q13_5_amount_mo Treatment BE_Q13_5_amount_mo BE_Q13_5_amount_mo_mdum InPanel `sc_controls', vce(cl cluster)

* Table 5: Labor Supply
regress Q12_2 Treatment `sc_controls', vce(cl cluster)
regress Q5_frac_working Treatment BE_Q5_frac_working BE_Q5_frac_working_mdum InPanel `sc_controls', vce(cl cluster)
regress Q10_4_tot_family Treatment BE_Q10_4_tot_family BE_Q10_4_tot_family_mdum InPanel `sc_controls', vce(cl cluster)

* Table 6: Assets and Weekly Expenditures
regress Q9_3_totalcategories Treatment `sc_controls', vce(cl cluster)
regress Q9_totvalue Treatment `sc_controls', vce(cl cluster)
regress Q7_nonfood_nondur Treatment `sc_controls', vce(cl cluster)
regress foodconsump Treatment BE_foodconsump BE_foodconsump_mdum InPanel `sc_controls', vce(cl cluster)
regress Q18_2_2_amount_wk Treatment `sc_controls', vce(cl cluster)
regress Q18_2_1_amount_wk Treatment `sc_controls', vce(cl cluster)
regress Q7_tempt Treatment `sc_controls', vce(cl cluster)
regress Q18_2_3_amount_wk Treatment `sc_controls', vce(cl cluster)

* Table 7: Social Effects
regress Q5_fraction Treatment BE_Q5_fraction BE_Q5_fraction_mdum InPanel `sc_controls', vce(cl cluster)
regress Q16_tot_somepower_cond Treatment `sc_controls', vce(cl cluster)
regress Q16_tot_aconf_cond Treatment `sc_controls', vce(cl cluster)
regress Q2_3_sing_any Treatment `sc_controls', vce(cl cluster)
regress Q15_2_mean_formal Treatment `sc_controls', vce(cl cluster)
regress Q15_2_mean_people Treatment `sc_controls', vce(cl cluster)
regress Q20_2 Treatment BE_Q20_2 BE_Q20_2_mdum InPanel `sc_controls', vce(cl cluster)

* Table 8: Various Measures of Welfare
regress Q22_mean Treatment BE_Q22_mean BE_Q22_mean_mdum InPanel `sc_controls', vce(cl cluster)
regress stress_index Treatment `sc_controls', vce(cl cluster)
regress Q17_AverageLocus Treatment `sc_controls', vce(cl cluster)
regress sat_index Treatment `sc_controls', vce(cl cluster)
regress Q12_1_2_sat Treatment `sc_controls', vce(cl cluster)
regress Q3_1_sat Treatment BE_Q3_1_sat BE_Q3_1_sat_mdum InPanel `sc_controls', vce(cl cluster)
regress Q9_4_soldloan_none Treatment `sc_controls', vce(cl cluster)
regress Q9_4_anycategories Treatment `sc_controls', vce(cl cluster)
